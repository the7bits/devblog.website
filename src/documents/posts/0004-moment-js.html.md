---
title: 'Moment.js'
author: 'Олег Корх'
layout: post
tags: ['javascript','coffeescript','time','date']
date: '2013-01-12'
---

[Moment.js](http://momentjs.com)  
Потрясающая маленькая но функциональная библиотека для работы с датами в JavaScript. Можно использовать как в браузере так и в Node.js.

Вот так например она используется в данном блоге:

    formatDate: (date,format='LL') -> 
                moment = require('moment')
                moment.lang('ru')
                moment(date).format(format)

Это в файле `docpad.coffee`. И соответственно в шаблоне:

    <%= @formatDate(@document.date) %>

Документация на сайте библиотеки очень доступная, с примерами, сразу видно как использовать.
