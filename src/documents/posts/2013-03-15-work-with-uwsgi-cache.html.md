---
title: 'Работа с uwsgi-cache'
author: 'Снигирь Константин'
layout: post
tags: ['django', 'python', 'uwsgi', 'uwsg-cache']
date: '2013-03-15'
---

Хотелось бы написать пару слов о простой настройке [WebCaching Framework](http://uwsgi-docs.readthedocs.org/en/latest/WebCaching.html)
при запуске Django-проекта в связке uwsgi + ngnix.
Допустим, у Вы уже установили и uwsgi, ngnix, virtualenviroment и Django (Django 1.4).
Создадим для демонстрации проект tuwsgi

     $ django-admin.py startproject tuwsgi

И создадим в проекте приложение cachetest

     $ cd tuwsgi
     $ ./manage.py startapp cachetest

<!-- Read more -->

Для того, чтобы использовать uwsgi-cache, как backend, нужно добавить  файл `uwsgicachebackend.py` в папку с `settings.py` и наполнить его следующим содержимым

    #uwsgicachebackend.py
    """uWSGI cache backend"""

    from django.core.cache.backends.base import BaseCache, InvalidCacheBackendError
    from django.utils.encoding import smart_str

    try:
        import cPickle as pickle
    except ImportError:
        import pickle

    try:
        import uwsgi
    except:
        raise InvalidCacheBackendError(
            "uWSGI cache backend requires you are running under it to have the 'uwsgi' module available")


    class UWSGICache(BaseCache):
        def __init__(self, server, params):
            BaseCache.__init__(self, params)
            self._cache = uwsgi
            self._server = server

        def exists(self, key):
            return self._cache.cache_exists(smart_str(key), self._server)

        def add(self, key, value, timeout=0, version=None):
            if self.exists(key):
                return False
            return self.set(key, value, timeout, self._server)

        def get(self, key, default=None, version=None):
            val = self._cache.cache_get(smart_str(key), self._server)
            if val is None:
                return default
            val = smart_str(val)
            return pickle.loads(val)

        def set(self, key, value, timeout=0, version=None):
            self._cache.cache_update(smart_str(key), pickle.dumps(value), timeout, self._server)

        def delete(self, key, version=None):
            self._cache.cache_del(smart_str(key), self._server)

        def close(self, **kwargs):
            pass

        def clear(self):
            self._cache.reload()


    # For backwards compatibility
    class CacheClass(UWSGICache):
        pass

На самом деле это копия кода с документации с небольшой модификацией - добавление очищения кеша

     def clear(self):
        self._cache.reload()

По правде говоря, неплохо было бы еще выяснить, как реализовать функцию close(), так как на данный момент там стоит заглушка,
но это несколько затруднительно, так как нельзя порсто взять и написать

    import uwsgi
    print dir(uwsgi)

так, как модуль `uwsgi` добавляется проецируется в `PYTHONPATH` только во время работы cache.
Но я постараюсь исправить эту недоработку в ближайшее время.

Теперь осталось научить наш проект использовать этот кеш.

Добавим в `settings.py` строчки

    try:
        import uwsgi
        UWSGI = True
    except:
        UWSGI = False

    if UWSGI:
        CACHES = {
            'default': {
                'BACKEND': 'tuwsgi.uwsgicachebackend.UWSGICache',
                'OPTIONS': {
                    'MAX_ENTRIES': uwsgi.opt['cache'],
                    'LOCATION': 'unix:///tmp/uwsgi.sock'
                }
            }
        }

В блок `try ... except` оборачиваем для того, чтобы потом при использовании `manage.py` команд мы не получили ошибку импорта модуля.
Особеннов внимание стоит уделить опции `LOCATION` . Стоит запомнить ее значение, это сокет, через который приложения будут
обмениваться информацией с кешом. Адрес сокета нам будет нужен при настройке связки uwsgi и nginx.

Идем дальше. Создадим файл `/etc/nginx/sites-available/tuwsgi` и впишем в него настройки сервера

    server {
		listen  80; #Порт сервера
		server_name tuwsgi; #Имя сервера
		access_log /var/log/nginx/tuwsgi_access.log;
		error_log /var/log/nginx/tuwsgi_error.log;
		# Защита от раздачи одинаковой куки в кешированном ответе
		proxy_hide_header "Set-Cookie";
		# Игнорировать параметры кеша заданные бекэндом
		proxy_ignore_headers "Cache-Control" "Expires";
		location /favicon.ico {
			return 404;
		}
		location / {
			include     uwsgi_params;
			uwsgi_pass  app;
			expires -1;
		}

		location /media/  {
			add_header Cache-Control public; #Кеширование медиа на стороне клиента
		    alias /home/lex/Projects/7bits/testwsgi/tuwsgi/media/; # Путь в Вашей media
		}

		location  /static/ {
			expires 4M;
			#Кешируем везде (и на прокси и на клиентах)
			add_header Cache-Control public;
		    alias  /home/lex/Projects/7bits/testwsgi/tuwsgi/static/;# Путь в Вашей static
		}
	}

Обратите внимание на строчку

    uwsgi_pass  app;

Теперь откроем файл `/etc/nginx/nginx.conf` и настроим сокет для обмена. Для этого нужно в секцию `http` добавить строки

    upstream app {
		# Distribute requests to servers based on client IP. This keeps load
		# balancing fair but consistent per-client. In this instance we're
		# only using one uWGSI worker anyway.
		ip_hash;
		server unix:///tmp/uwsgi.sock;
	}

Заметьте, что имена сокетов совпадают.

Осталось совсем немного. Создать симлинк созданного файла в `/etc/nginx/sites-enabled/tuwsgi` для того, чтобы сервер работал.
Теперь настроим uwsgi

Создадим файл `/etc/uwsgi/apps-available/tuwsgi.ini`, в который впишем настройки нашего проекта

    [uwsgi]
    vhost = true
    plugins = python27 #Ваша версия питона
    socket = /tmp/uwsgi.sock #Тот же сокет для обмена
    master = true
    enable-threads = true
    processes = 1
    virtualenv = /home/lex/.virtualenvs/testuwsgi # Путь к виртуальному окружению
    chdir = /home/lex/Projects/7bits/testwsgi/tuwsgi/ #Путь к проекту
    module = tuwsgi.wsgi #Refrence wsgi-файла
    touch-reload = /home/lex/Projects/7bits/testwsgi/tuwsgi/reload #Файл, которому можно делать touche reload для перегрузки кода без перезагрузки сервера
    http = :80
    # advanced server configuration
    buffer-size     = 32768 #Размер буфера
    disable-logging = true
    enable-threads  = true
    harakiri        = 60
    cache = 1000 # Количество елементов в кеше
    cache-blocksize = 65536 #Размер блока для хранения записи

Это самая простая конфигурация. Если Вам нужна более гибкая настройка, список доступных опций можно посмотреть [тут](https://uwsgi-docs.readthedocs.org/en/latest/Options.html#cache).
Окей, теперь сделаем симлинк этого файла в `/etc/uwsgi/apps-enabled/tuwsgi.ini` и можно все рестартовать.

    sudo service nginx restart
    sudo service uwsgi restart

В файле `views.py` нашего приложения cachetest пишем

    from django.http import HttpResponseRedirect
    from django.template import RequestContext
    from django.core.cache import cache
    from django.shortcuts import render_to_response


    def main(request):
        if request.method == "POST":
            if not cache.exists('test_key'):
                cache.add('test_key', request.POST.get('new_val'))
            else:
                cache.set('test_key', request.POST.get('new_val'))
        value = cache.get('test_key')
        return render_to_response('base.html', {'value': value}, context_instance=RequestContext(request))


    def clear_cache(request):
        cache.clear()
        return HttpResponseRedirect('/')

И пишем шаблон `base.html`

    <!DOCTYPE html>
    <html>
        <head>
            <title>Test page</title>
        </head>
        <body>
            <center>
                    <b>UWSGI cache test</b>
            </center>
            <br />
            <br /><br /><br /><br /><br />
            <center>
                <b>Current value in cache:</b>
                {{ value }}
            </center>
            <form method="post" action="/">
                {% csrf_token %}
                <p>New value in cache<input type="text" name="new_val"></p>
                <p><input type="submit" value="Change"></p>
            </form>
            <a href="/clear/">Clear uwsgi cache</a>
        </body>
    </html>

И в `urls.py` нашего проекта настроим роутинги

    from django.conf.urls import patterns, include, url

    # Uncomment the next two lines to enable the admin:
    # from django.contrib import admin
    # admin.autodiscover()

    urlpatterns = patterns('',
        # Examples:
        # url(r'^$', 'tuwsgi.views.home', name='home'),
        # url(r'^tuwsgi/', include('tuwsgi.foo.urls')),

        # Uncomment the admin/doc line below to enable admin documentation:
        # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

        # Uncomment the next line to enable the admin:
        # url(r'^admin/', include(admin.site.urls)),
        url(r'^$', 'cachetest.views.main'),
        url(r'^clear/$', 'cachetest.views.clear_cache', name='c_clear'),
    )

Теперь мы имеем страницу, в которой будем отображать текущее значение из кеша, и формочку, с помощью которой сможем его менять.

Все, мы имеем шустрый кеш без установки `memcached`, в который можно пихать все, что угодно. Также можно кешировать страницы, вьюхи и прочие тяжелые вещи, которые меняются не очень часто.
Да прибудет с Вами сила.