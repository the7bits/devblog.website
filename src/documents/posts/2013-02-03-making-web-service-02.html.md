---
title: 'Делаем веб-сервис. Часть 2'
author: 'Олег Корх'
layout: post
tags: ['django', 'python', 'service', 'part_2']
date: '2013-02-03'
---

Появилось немного времени, поэтому продолжу цикл статей о разработке веб-сервисов на Django. В [предыдущей части](/posts/2013-01-20-making-web-service-01) мы создали базовый скелет проекта нашего веб-сервиса. 

Итак, займемся системой аутентификации, причем подойдем к вопросу серьезно, так чтобы созданную нами систему, можно было потом использовать и в следующих проектах. В Django 1.5 подсистема аутентификации претерпела серьезные изменения в сравнении с предыдущими релизами (возможно даже это одно из наибольших усовершенствований во всем фреймворке). Самое главное --- это то, что теперь можно описывать собственные модели пользователей вместо использования стандартной `django.contrib.auth.models.User`, что существенно упрощает многие вещи (даже если в начале изучения фреймворка так и не кажется).

Первым делом, установим [South](http://south.readthedocs.org/en/0.7.6/). Данная библиотека является стандартом де-факто среди разработчиков на Django для поддержки миграций баз данных. Она понадобится нам еще не раз, поскольку при разработке относительно больших проектов, вы никогда не сможете заранее описать структуру БД такой какой она должна быть. South позволит сэкономить огромное количество времени позволяя изменять структуру БД не боясь о сохранности данных.

Само собой, что перед установкой библиотек (да и вообще, для работы) нужно не забыть активировать соответствующее виртуальное окружение.

    $ source env_camomile/bin/activate
    $ pip install south
    
<!-- Read more -->

Теперь создадим новое приложение, которое будет отвечать за работу с пользователями. Назовем его `accounts`.

    $ ./manage.py startapp accounts
    
И сразу же добавим его в список `INSTALLED_APPS` в `settings.py`, а также запустим команду `./manage.py syncdb` (South хранит в БД информацию о примененных миграциях). 

Очень важно стараться по возможности создавать переносимые приложения, Django это поощряет, а кроме того, если приложения будут переносимыми, то разработка последующих проектов будет намного быстрее.

Определимся с тем, что нам нужно от нашей системы аутентификации:

* использование адреса электронной почты для логина;
* поддержка ников пользователей;
* необходимость активации аккаунта путем отправки на почту соответствующего сообщения с урлом для подтверждения правильности адреса;
* возможность разграничения прав доступа.

Еще один важный момент --- будем по возможности стараться писать код совместимый как с Python 2.x так и с Python 3.x. Конечно сейчас, когда еще большая часть библиотек Django не портирована на Python 3, мы продолжаем использовать вторую ветку, но чтобы в дальнейшем у нас было как можно меньше проблем с переходом на третью ветку (а в Django 1.6 уже будет полноценная поддержка), то будем пробовать писать по возможности переносимый код. В этом нам поможет библиотека [six](http://packages.python.org/six/), доступ к которой можно получить через `django.utils`. Довольно подробно тема портирования кода на Python 3 описана в [официальной документации Django](https://docs.djangoproject.com/en/1.5/topics/python3/).

Итак, опишем собственную модель пользователя и соответствующий менеджер в `accounts/models.py`:

``` python
# coding: utf-8
from __future__ import unicode_literals

from django.db import models
from django.utils import six
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager,\
        PermissionsMixin

ALREADY_ACTIVATED = 'ALREADY_ACTIVATED'


class CustomerManager(BaseUserManager):
    """
    Creates and saves a Customer with the given email, nickname and password.
    """
    def create_user(self, email, nickname, password=None):
        """
        Creates and saves a Customer with the given email, nickname and password.
        """
        if not email:
            raise ValueError(_('Customers must have an email address'))

        customer = self.model(
            email=CustomerManager.normalize_email(email),
            nickname=nickname
        )
        customer.set_password(password)
        customer.save(using=self._db)
        return customer

    def create_superuser(self, email, nickname, password):
        """
        Creates and saves a superuser with the given email, nickname and password.
        """
        customer = self.create_user(email, nickname, password)
        customer.is_admin = True
        customer.save(using=self._db)
        return customer


@python_2_unicode_compatible
class Customer(AbstractBaseUser, PermissionsMixin):
    """
    A model which implements the authentication model.

    Email, password and nickname are required. Other fields are optional.

    Email field is used for logging in.
    """
    email = models.EmailField(
        _('Email'), max_length=255, unique=True)
    nickname = models.CharField(
        _('Nickname'), max_length=50, unique=True)

    is_admin = models.BooleanField(_('Admin status'), default=False)
    is_active = models.BooleanField(_('Active'), default=True)

    date_joined = models.DateTimeField(_('Date joined'), default=timezone.now)
    activation_code = models.CharField(_('Activation code'), max_length=255)

    objects = CustomerManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['nickname']

    class Meta:
        verbose_name = _('Customer')
        verbose_name_plural = _('Customers')
    
    def __str__ (self):
        return self.nickname

    def get_full_name(self):
        return self.nickname

    def get_short_name(self):
        return self.nickname

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin
```       
    
Также добавим в `settings.py` следующую строку:

    AUTH_USER_MODEL = 'accounts.Customer'

Что же мы сделали? Мы описали новую модель `Customer` согласно наших представлений о том как должна выглядеть учетная запись пользователя (в сравнении с `django.contrib.auth.models.User` нам не нужны поля `first_name`, `last_name`, а также мы хотим логиниться по электронной почте). Класс `AbstractBaseUser` --- это абстрактный класс от которого принято создавать собственные модели пользователей. `PermissionsMixin` --- примесь добавляющая учет прав доступа. Также мы описали собственный менеджер для работы с пользователями. Конечно, данный код еще будет дорабатываться, например система прав доступа пока является фактически отключенной (необходимые методы просто возвращают `True`), функция активации пока также еще не реализована и т.д.

Можно посмотреть, как будет выглядеть SQL код для создания новых таблиц в БД:

    $ ./manage.py sqlall accounts
    BEGIN;
    CREATE TABLE "accounts_customer_groups" (
        "id" serial NOT NULL PRIMARY KEY,
        "customer_id" integer NOT NULL,
        "group_id" integer NOT NULL REFERENCES "auth_group" ("id") DEFERRABLE INITIALLY DEFERRED,
        UNIQUE ("customer_id", "group_id")
    )
    ;
    CREATE TABLE "accounts_customer_user_permissions" (
        "id" serial NOT NULL PRIMARY KEY,
        "customer_id" integer NOT NULL,
        "permission_id" integer NOT NULL REFERENCES "auth_permission" ("id") DEFERRABLE INITIALLY DEFERRED,
        UNIQUE ("customer_id", "permission_id")
    )
    ;
    CREATE TABLE "accounts_customer" (
        "id" serial NOT NULL PRIMARY KEY,
        "password" varchar(128) NOT NULL,
        "last_login" timestamp with time zone NOT NULL,
        "is_superuser" boolean NOT NULL,
        "email" varchar(255) NOT NULL UNIQUE,
        "nickname" varchar(50) NOT NULL UNIQUE,
        "is_admin" boolean NOT NULL,
        "is_active" boolean NOT NULL,
        "date_joined" timestamp with time zone NOT NULL,
        "activation_code" varchar(255) NOT NULL
    )
    ;
    ALTER TABLE "accounts_customer_groups" ADD CONSTRAINT "customer_id_refs_id_1b68d15c" FOREIGN KEY ("customer_id") REFERENCES "accounts_customer" ("id") DEFERRABLE INITIALLY DEFERRED;
    ALTER TABLE "accounts_customer_user_permissions" ADD CONSTRAINT "customer_id_refs_id_006369da" FOREIGN KEY ("customer_id") REFERENCES "accounts_customer" ("id") DEFERRABLE INITIALLY DEFERRED;
    
    COMMIT;
    
Как видите, Django добавил ряд сервисных полей, кроме того работа с БД с помощью ORM существенно проще чем через SQL. Теперь можно создавать первую миграцию:

    $ ./manage.py schemamigration --initial accounts

Вообще, на данном этапе, проще всего нам будет полностью обнулить базу данных, поскольку мы полностью переработали систему аутентификации и нам не нужны в БД старые таблицы. Поэтому сначала, например с помощью pgAdmin удаляем все таблицы, а потом делаем следующее:

    $ ./manage.py syncdb --all
    $ ./manage migrate --fake

При этом обратите внимание, что после создания таблиц Django предложит создать суперпользователя, но теперь он будет задавать вопросы уже относительно новой структуры.

Если теперь зайти в админку (уже используя электронную почту и пароль), то можно увидеть, что возможность управления пользователями пропала. Из старой системы аутентификации остался только раздел `Группы`. Поэтому следующим шагом займемся настройкой админки. Причем нам необходимо будет также создать необходимые формы для работы c записями пользователей. Но сначала создадим файл `accounts/admin.py`:

``` python
# coding: utf-8
from django.contrib import admin
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib.auth.admin import UserAdmin

from accounts.models import Customer

class CustomerAdmin(UserAdmin):
    list_display = ('nickname', 'email', 'is_admin')
    list_filter = ('is_admin', 'is_superuser', 'is_active', 'groups')
    search_fields = ('nickname', 'email')
    ordering = ('nickname',)
    filter_horizontal = ('groups', 'user_permissions',)

admin.site.register(Customer, CustomerAdmin)
```

Итак, мы взяли за основу класс `UserAdmin` и доработали его изменив названия отображаемых полей соответственно нашей новой структуре пользователей. Также, мы сразу же можем видеть в админке новый раздел управления пользователями.

![](https://dl.dropbox.com/u/381385/devblog/images/making-web-service-02/Selection_001.png)

Проблема состоит в том, что попытавшись открыть запись уже имеющегося пользователя или попытавшись создать нового, мы столкнемся с сообщением об ошибке. Дело в том, что для управления пользователями в админке используются кастомные формы описанные в `django.contrib.auth.forms`, и класс `CustomerAdmin` наследованный от `UserAdmin` также их использует. Но поскольку мы создали собственную модель пользователя, то старые формы не подходят. Поэтому, сначала создадим файл `accounts/forms.py`:

``` python
# coding: utf-8
from __future__ import unicode_literals

from django import forms
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from accounts.models import Customer

class CustomerChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField(label=_('Password'),
        help_text=_('Raw passwords are not stored, so there is no way to see '
                    'this user\'s password, but you can change the password '
                    'using <a href="password/">this form</a>.'))

    class Meta:
        model = Customer
    
    def __init__(self, *args, **kwargs):
        super(CustomerChangeForm, self).__init__(*args, **kwargs)
        f = self.fields.get('user_permissions', None)
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')

    def clean_password(self):
        return self.initial['password']
```

Итак, теперь если в класс `CustomerAdmin` дописать `form = CustomerChangeForm`, то в админке станет доступным функционал правки записи пользователя. Но поскольку, при попытке создания нового пользователя, мы все еще видим старую форму, то необходимо дописать еще немного кода.

``` python
class CustomerCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given email, nickname and
    password.
    """
    error_messages = {
        'duplicate_email': _("A user with that email already exists."),
        'duplicate_nickname': _("A user with that nickname already exists."),
        'password_mismatch': _("The two password fields didn't match."),
    }
    password1 = forms.CharField(label=_("Password"),
        widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Password confirmation"),
        widget=forms.PasswordInput,
        help_text=_("Enter the same password as above, for verification."))

    class Meta:
        model = Customer
        fields = ('email', 'nickname',)

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            Customer.objects.get(email=email)
        except Customer.DoesNotExist:
            return email
        raise forms.ValidationError(self.error_messages['duplicate_email'])


    def clean_nickname(self):
        nickname = self.cleaned_data['nickname']
        try:
            Customer.objects.get(nickname=nickname)
        except Customer.DoesNotExist:
            return nickname
        raise forms.ValidationError(self.error_messages['duplicate_nickname'])

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'])
        return password2

    def save(self, commit=True):
        customer = super(CustomerCreationForm, self).save(commit=False)
        customer.set_password(self.cleaned_data["password1"])
        if commit:
            customer.save()
        return customer
```
        
И окончательно подправим `CustomerAdmin`:

``` python
class CustomerAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'nickname', 'password', 'activation_code')}),
        (_('Permissions'), {'fields': ('is_active', 'is_admin', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'nickname', 'password1', 'password2')}
        ),
    )

    list_display = ('nickname', 'email', 'is_admin')
    list_filter = ('is_admin', 'is_superuser', 'is_active', 'groups')
    search_fields = ('nickname', 'email')
    ordering = ('nickname',)
    filter_horizontal = ('groups', 'user_permissions',)

    form = CustomerChangeForm
    add_form = CustomerCreationForm
```
    
Есть правда еще одна нестыковка --- при создании нового пользователя над формой в админке написан старый текст, который не совсем совместим с новой формой. Поэтому создадим новый шаблон `accounts/templates/accounts/admin//auth/user/add_form.html` следующего содержания:

    {% extends "admin/change_form.html" %}
    {% load i18n %}
    
    {% block form_top %}
      {% if not is_popup %}
        <p>{% trans "First, enter an email, nickname and password. Then, you'll be able to edit more user options." %}</p>
      {% else %}
        <p>{% trans "Enter an email, nickname and password." %}</p>
      {% endif %}
    {% endblock %}
    
    {% block after_field_sets %}
    <script type="text/javascript">document.getElementById("id_username").focus();</script>
    {% endblock %}
    
И добавим в класс `CustomerAdmin` следующее:

    add_form_template = 'accounts/admin/auth/user/add_form.html'
    
Теперь остается только добавить локализацию для нашего приложения. Начнем с русской локализации. Делается это очень просто, сначала создадим папку `accounts/locale`. Далее, находясь в папке приложения запускаем команду:

    $ django-admin.py makemessages -l ru
    
Откроем теперь созданный данной командой файл `accounts/locale/ru/LC_MESSAGES/django.po`. Это обычный текстовый файл в котором для каждой оригинальной строки (которая была заключена в функцию  `ugettext`) можно указать ее перевод. Для правки можно использовать любой текстовый редактор, но лучше конечно воспользоваться специализированным инструментом для перевода, как например [Poedit](http://www.poedit.net/).

![](https://dl.dropbox.com/u/381385/devblog/images/making-web-service-02/poedit.png)

При этом, если вы используете Poedit, то при сохранении он автоматически скомпилирует файл переводов в формат `.mo` который уже и будет использоваться Django. Если же использовать обычный текстовый редактор, то придется запустить еще одну команду:

    $ django-admin.py compilemessages
    
Итак, мы создали собственную модель пользователя используя новые инструменты Django 1.5, научили админку работать с данной моделью и даже немного разобрались с локализацией. Оставайтесь на связи, дальше будет интереснее.
    