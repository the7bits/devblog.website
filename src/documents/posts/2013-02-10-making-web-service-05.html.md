---
title: 'Делаем веб-сервис. Часть 5'
author: 'Олег Корх'
layout: post
tags: ['django', 'python', 'service']
date: '2013-02-10'
---

Итак, продолжим. В данной статье мы займемся реализацией системы инвайтов. 

Хорошей практикой считается как можно более быстрый запуск проекта в продакшн. Сделать это можно только в одном случае --- если сильно ограничить набор необходимого функционала. Например, мы приблизительно представляем какие фичи должны быть реализованы в продукте (например у нас есть список из полсотни пунктов), но понимаем, что реализация их всех займет много времени. В этом случае выбираем 1-2 самых главных фич, реализовываем их и запускаем проект. А далее уже говорим, что это бета-версия и занимаемся реализацией оставшихся фич постепенно, выкладывая их в продакшн по мере готовности.

Конечно, в тот момент когда сервис еще находится на самой ранней стадии разработки и имеет кучу багов, то разумнее не открывать публичную регистрацию, а реализовать систему инвайтов. Т.е. зарегистрироваться в сервисе смогут только те, кто будет явно приглашен. В нашем случае послать инвайт кому-либо можно будет через админку. Также мы предусмотрели кнопку `Request invite` на главной странице с помощью которой любой желающий сможет оставить адрес своей электронной почты для получения инвайта. Такой подход позволит регулировать рост аудитории, что очень важно на начальном периоде.

<!-- Read more -->

Систему инвайтов будем развивать на базе приложения `accounts`. Начнем с создания новой модели в `accounts/models.py`:

``` python
@python_2_unicode_compatible
class Invite(models.Model):
    """
    A model which implements the invite record.

    Email field is used for sending the mail.
    """
    email = models.EmailField(
        _('Email'), max_length=255, unique=True)
    code = models.CharField(_('Code'), max_length=50)

    is_sent = models.BooleanField(_('Sent'), default=False)
    is_active = models.BooleanField(_('Active'), default=True)

    date_requested = models.DateTimeField(_('Date requested'), default=timezone.now)
    date_used = models.DateTimeField(_('Date used'), default=timezone.now)
    registered_customer = models.OneToOneField(Customer, null=True, blank=True)

    class Meta:
        verbose_name = _('Invite')
        verbose_name_plural = _('Invites')

    def __str__(self):
        return six.text_type(self.email)
```

Смысл атрибутов модели следующий:

* `email` --- адрес электронной почты на который будет выслан инвайт;
* `code` --- специальный код используемый для проверки инвайта;
* `is_sent` --- `True` если инвайт отослан на почту;
* `is_active` --- `True` если по инвайту еще никто не зарегистрировался;
* `date_requested` --- дата создания записи об инвайте;
* `date_used` --- дата использования инвайта;
* `registered_customer` --- ссылка на пользователя зарегистрировавшегося по данному инвайту.

Теперь, создадим новую миграцию:

    $ ./manage.py schemamigration --auto accounts

И применяем ее:

    $ ./manage.py migrate accounts
    
Также, необходимо внести правки в файл `terrains.py`. Дело в том, что в том виде в котором он сейчас у нас есть с ним есть большая проблема --- после каждого прогона тестовых сценариев основная база данных сбрасывается к изначальному состоянию. Причем сбрасывается также информация о примененных миграциях. Все это уже сейчас начинает нам мешать, поэтому добавим немного кода чтобы реализовать автоматическое создание тестовой БД и прогон сценариев именно на ней, а не на основной базе. Добавим следующий код:

``` python
import logging

logger = logging.getLogger(__name__)
world.old_database_name = None

@before.runserver
def prepare_database(server):
    from south.management.commands import patch_for_test_db_setup
    from django.db import connection
    from django.core.management import call_command
    from django.conf import settings
   
    logger.info("Setting up a test database ...\n")    
    patch_for_test_db_setup()
    world.old_database_name = settings.DATABASES["default"]["NAME"]
    connection.creation.create_test_db()
    call_command('syncdb', interactive=False, verbosity=0)

@after.runserver
def teardown_database(server):
    from django.db import connection
    logger.info("Destroying test database ...\n")
    connection.creation.destroy_test_db(world.old_database_name)
```

Теперь, настроим админку, чтобы через нее можно было хотя бы пока следить за инвайтами:

``` python
class InviteAdmin(admin.ModelAdmin):
    pass

admin.site.register(Invite, InviteAdmin)
```

Начнем с запроса инвайта по нажатию на кнопку `Request invite`. Создадим файл `request_invite.feature` и опишем в нем сценарий для данной фичи.

```
Feature: Request invite
    In order to request the invite
    As a user
    I want to press the button and type data for requesting the invite

    Scenario: Open the index page as anonymous user
        Given I visit the django url "/accounts/request-invite/"
        When I fill in the field named "email" with "test@testmail.com"
        And I click on the button named "submit"
        Then I should see the element with the css selector ".alert-success"
        And I should see an email is sent to "test@testmail.com" with subject "Your request is added to queue"
```

Данный сценарий хорошо читается. Согласно нему мы должны увидеть форму с полем ввода в которое можно было бы ввести адрес своей электронной почты, после чего мы должны увидеть сообщение об успешной отправке сообщения и собственно получить почту в почтовый ящик с заданной темой.

Конечно же, первым делом при попытке прогона сценария мы получим сообщение, что шаг с проверкой почты не определен, поэтому добавим в `common_steps.py` следующую функцию:

``` python
@step(u'an email is sent to "([^"]*?)" with subject "([^"]*)"')
def email_sent(step, to, subject):
    message = mail.queue.get(True, timeout=5)
    assert message.subject == subject
    assert to in message.recipients()
```

Теперь, когда тестовый сценарий описан, можем приступать к реализации. Сначала, опишем новую форму для ввода адреса электронной почты:

``` python
class InviteCreationForm(forms.ModelForm):
    class Meta:
        model = Invite
        fields = ('email',)
```

Далее идем в `accounts/views.py` и создаем соответствующее отображение для создания новой записи инвайта. Есть много различных подходов к написанию отображений, но мы будем стараться по возможности использовать [Generic Class Based Views](https://docs.djangoproject.com/en/1.5/ref/class-based-views/generic-editing/#django.views.generic.edit.CreateView). Данный подход позволяет заметно уменьшить объем кода.

``` python
# coding: utf-8
from __future__ import unicode_literals

from django.utils.translation import ugettext as _
from django.views.generic.edit import CreateView

from accounts.forms import InviteCreationForm

class InviteCreate(CreateView):
    form_class = InviteCreationForm
    template_name = 'accounts/invite_new.html'
    success_url = 'success'
```

А также добавляем в `accounts/urls.py` ссылку на новое отображение:

``` python
from accounts.views import InviteCreate

...
    url(r'^request-invite/$', InviteCreate.as_view(), name='request_invite'),
...
```

Также, создадим шаблон `accounts/invite_new.html`:

```
{% extends "base.html" %}
{% load i18n %}

{% block content %}
<h3>{% trans "Request invite" %}</h3>
<form method="post">
{% csrf_token %}
{{ form.as_ul }}
<input type="submit" name="submit" value="{% trans 'Submit' %}" />
</form>
{% endblock %}
```

В результате при нажатии кнопки `Request invite` мы увидим следующую форму:

![](https://dl.dropbox.com/u/381385/devblog/images/making-web-service-05/Selection_007.png)

Попробуем немного приукрасить форму, сделать ее в стиле Twitter Bootstrap. Для этого воспользуемся библиотекой [django-bootstrap-toolkit](https://github.com/dyve/django-bootstrap-toolkit):

    $ pip install django-bootstrap-toolkit
    
И добавим в `INSTALLED_APPS`:

    'bootstrap_toolkit',
    
Теперь просто подключим в шаблоне с формой новую библиотеку `{% load bootstrap_toolkit %}` и отобразим форму:

```
<form method="post" class="form-vertical">
{% csrf_token %}
{{ form|as_bootstrap:"vertical" }}
<div class="form-actions">
<input class="btn btn-primary" type="submit" name="submit" value="{% trans 'Submit' %}" />
</div>
</form>
```

Теперь форма выглядит уже следующим образом:

![](https://dl.dropbox.com/u/381385/devblog/images/making-web-service-05/Selection_008.png)

Итак, теперь создадим еще один шаблон `accounts/invite_success.html` в котором напишем сообщение об успешном получении запроса на инвайт:

```
{% extends "base.html" %}
{% load i18n %}

{% block content %}
<h3>{% trans "Success" %}</h3>
<div class="alert alert-success">
{% blocktrans %}
Your request is successfully received. We'll send you invite as soon as possible.
{% endblocktrans %}
</div>
{% endblock %}
```

И добавим в `accounts/urls.py` еще одну строку:

``` python
    url(r'^request-invite/success/$', TemplateView.as_view(template_name="accounts/invite_success.html")),
```

Теперь после указания адреса электронной почты мы сможем увидеть соответствующее сообщение:

![](https://dl.dropbox.com/u/381385/devblog/images/making-web-service-05/Selection_009.png)

Далее нам необходимо реализовать две вещи:

* генерацию кода инвайта при создании новой записи;
* отправку сообщения на указанный почтовый адрес о том, что электронная почта пользователя добавлена в очередь ожидания на рассылку инвайтов.

Чтобы автоматически сгенерировать код и заполнить поле `code` в модели `Invite` при создании нового объекта --- воспользуемся системой сигналов.

``` python
from django.db.models.signals import post_save
from django.dispatch import receiver

@receiver(post_save, sender=Invite)
def invite_save_handler(sender, instance, created, **kwargs):
    if created:
        import string
        import random

        size = 50   # code.max_length
        allowed = string.ascii_letters
        randomstring = ''.join([allowed[random.randint(0, len(allowed) - 1)] \
                for x in xrange(size)])
        
        instance.code = randomstring
        instance.save(update_fields=['code'])
```

Теперь нужно реализовать отправку почтовых сообщений. При этом отправлять почту будем как в формате HTML, так и в простом текстовом (чтобы их могли прочитать пользователи почтовых клиентов которые отключили прием писем в формате HTML). Поскольку рассылка почты является одним из наиболее часто используемых компонентов, то логично вынести ее в отдельное приложение.

    $ ./manage.py startapp mail
    
Пока поступим просто --- создадим в рамках нового приложения модуль `utils.py` и внутри него функцию для отправки почты:

``` python
# coding: utf-8
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives

def send_mail(to_emails, from_email, subject, 
    text_template='mail/message.html',
    html_template='mail/message.txt',
    data={}):
    """
    Function for sending email both in HTML and text formats.

    to_emails - list of emails for sending
    from_email - sender address
    subject - the subject of message
    text_template - template for text mail
    html_template - template for html mail
    data - dictionary with data for templates
    """
    html_content = render_to_string(html_template, data)
    text_content = render_to_string(text_template, data)
    msg = EmailMultiAlternatives(subject, text_content, from_email, to_emails)
    msg.attach_alternative(html_content, "text/html")
    msg.send()
```
И создадим несколько базовых шаблонов для писем, просто чтобы новое приложение можно было использовать.

В качестве базового шаблона для HTML писем воспользуемся качественным шаблоном [HTML Email Boilerplate](http://htmlemailboilerplate.com/). Также создадим шаблон и для обычных текстовых писем. Приводить их здесь я не буду, можно при желании посмотреть в репозитории.

Теперь добавим отправку письма в функцию `invite_save_handler`:

``` python
REQUEST_INVITE_TEXT = _('''
Thank you for your interest to our product. 

We'll send you the invite as soon as possible.
''')

...
        send_mail(to_emails=[instance.email],
                from_email=settings.DEFAULT_FROM_EMAIL,
                subject=_('Your request is added to queue'),
                data={'content':REQUEST_INVITE_TEXT})

```

Если теперь запустить на исполнение сценарии, то проход будет успешным. Это значит, что функциональность реализована.

