---
title: 'Делаем веб-сервис. Часть 4'
author: 'Олег Корх'
layout: post
tags: ['django', 'python', 'service', 'part_4']
date: '2013-02-07'
---

Продолжаем серию статей описывающей процесс разработки одного веб-сервиса. Сегодня приступим к облагораживанию внешнего вида страниц.

Первым делом скачаем [Twitter Bootstrap](http://twitter.github.com/bootstrap/index.html). Данные из распакованого архива положим в папку `vendor` (предварительно ее создав) внутри `camomile/static`.

Далее скачаем набор шрифтовых иконок [Font Awesome](http://fortawesome.github.com/Font-Awesome/) и также распакуем архив в папку `vendor`. Также в эту же папку будем складывать и другие необходимые библиотеки, здесь особо уже внимание акцентировать не буду.

Базовый шаблон для веб страниц `base.html` уже создавался, но его содержание необходимо полностью переработать. Опять же, далее я буду акцентировать внимание только на наиболее важных моментах, а все остальные исходники легко можно просмотреть в официальном [репозитории](https://bitbucket.org/the7bits/camomile).

<!-- Read more -->

Итак, начнем сначала с главной страницы. Для незалогиненного пользователя она будет очень проста --- какая-то картинка и всего две кнопки, `Логин` и `Запросить инвайт`. Согласно методике BDD сначала напишем тестовый сценарий (добавим его в приложение `core`):

    Feature: Index page
        In order have a full featured index page
        As a user
        I want to see buttons on it
    
        Scenario: Open the index page as anonymous user
            Given I visit the django url "/"
            When I look around
            Then I should see the link to "/accounts/login/"
            And I should see the link to "/accounts/request-invite/"

Теперь собственно займемся реализацией. Как и в случае создания базового шаблона для этого блога, я просто использовал код одного из примеров Bootstrap. Итак, `base.html`:

```
{% load i18n %}
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>{% block title %}Camomile{% endblock %}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Styles -->
    <link href="{{ STATIC_URL }}vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ STATIC_URL }}vendor/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="{{ STATIC_URL }}vendor/fontawesome/css/font-awesome.min.css" rel="stylesheet">
    <!--[if IE 7]>
    <link rel="stylesheet" href="{{ STATIC_URL }}vendor/fontawesome/css/font-awesome-ie7.min.css">
    <![endif]-->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <style type="text/css">
        body {
        padding-top: 20px;
        padding-bottom: 40px;
        }

        /* Custom container */
        .container-narrow {
        margin: 0 auto;
        max-width: 700px;
        }
        .container-narrow > hr {
        margin: 30px 0;
        }

        /* Main marketing message and sign up button */
        .jumbotron {
        margin: 60px 0;
        text-align: center;
        }
        .jumbotron h1 {
        font-size: 72px;
        line-height: 1;
        }
        .jumbotron .btn {
        font-size: 21px;
        padding: 14px 24px;
        }
    </style>
  </head>
  <body>
    <div class="container-narrow">
    {% block content %}{% endblock %}    
    </div> <!-- /container -->
    <!-- Import javascript -->
    <script src="{{ STATIC_URL }}vendor/jquery.js"></script>
    <script src="{{ STATIC_URL }}vendor/holder.js"></script>
    <script src="{{ STATIC_URL }}vendor/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
```

После этого можно дорабатывать `index.html`:

```
{% extends "base.html" %}
{% load i18n %}
{% block content %}
    <div class="jumbotron">
    <img src="holder.js/700x300">
    </div>

    {% if user.is_authenticated %}
        <span id="nickname-info">{{ user.nickname }}</span>
    {% else %}
        <div class="row-fluid">
        <div class="span3 offset3">
        <a class="btn btn-success btn-large btn-block" href="/accounts/login/">{% trans "Log in" %}</a>
        </div>
        <div class="span3">
        <a class="btn btn-warning btn-large btn-block" href="/accounts/request-invite/">{% trans "Request invite" %}</a>
        </div>
        </div>
    {% endif %}
{% endblock %}
```

Обратите внимание на использование библиотеки [holder.js](http://imsky.github.com/holder/), которая позволяет легко генерировать изображения -заглушки. Если сейчас прогнать тестовый сценарий, то он завершится успешно. Страница выглядит так:

![](https://dl.dropbox.com/u/381385/devblog/images/making-web-service-04/Selection_003.png)

Следующее, что необходимо реализовать --- эту же страницу, но уже для залогиненного пользователя. В этом случае разместим на ней приветствие и кнопку для перехода на главную страницу управления сайтами, пусть для этого будет использоваться адрес `/dashboard/`. Начнем опять же со сценария:

    Scenario: Open the index page as authorized user
        Given I log in as user "admin@testmail.com" and password "123456"
        And I visit the django url "/"
        When I look around
        Then I should see that the element with id "nickname-info" contains exactly "admin"
        And I should see the link to "/dashboard/"

Теперь при запуске сценариев мы получим сообщение, что следующий шаг не определен (данный пункт в логе будет указан коричневым цветом):

    Given I log in as user "admin@testmail.com" with password "123456"

А чуть далее будет предложено описать шаг с примером:

    You can implement step definitions for undefined steps with these snippets:
    
    # -*- coding: utf-8 -*-
    from lettuce import step
    
    @step(u'Given I log in as user "([^"]*)" with password "([^"]*)"')
    def given_i_log_in_as_user_group1_with_password_group2(step, group1, group2):
        assert False, 'This step must be implemented'
    
На этом примере можно видеть важный момент при использовании BDD, если вам не хватает стандартных шагов описанных в том же Salad, то вы просто берете и описываете собственный шаг так как считаете нужным. При этом, сначала мы пишем соответствующую фразу в сценарии, а потом приступаем к ее реализации. Поскольку логиниться под разными пользователями для тестовых задач нам придется часто, а также в рамках тестовых сценариев разных приложений, то мы создадим новый модуль для описания общих шагов. Создадим файл `common_steps.py` в той же папке где находится `manage.py` и `terrain.py`, и напишем туда следующее:

```
# coding: utf-8
from lettuce import step, world
from lettuce.django import django_url

@step(u'log in as user "(.*)" with password "(.*)"')
def log_in_as_user(step, username, password):
    world.browser.visit(django_url('/accounts/login/'))
    world.browser.fill('username', username)
    world.browser.fill('password', password)
    world.browser.find_by_css('input[type=submit]').click()
```

Здесь использовались специальные методы библиотеки Splinter которая позволяет управлять действиями в веб-браузере. Объект `lettuce.world.browser` инициализируется библиотекой Salad, которая снимает с нас большую часть работы по организации тестового окружения.

Чтобы описанный шаг стал доступен в наших сценариях необходимо еще добавить в `terrain.py` следующее:

    from common_steps import *
    
Теперь при попытке прогона сценариев мы увидим следующее:
    
        And I should see the link to "/dashboard/"                                          # home/bum/Projects/camomile/env_camomile/local/lib/python2.7/site-packages/salad/steps/browser/elements.py:20
        Traceback (most recent call last):
          File "/home/bum/Projects/camomile/env_camomile/local/lib/python2.7/site-packages/lettuce/core.py", line 143, in __call__
            ret = self.function(self.step, *args, **kw)
          File "/home/bum/Projects/camomile/env_camomile/local/lib/python2.7/site-packages/salad/steps/browser/elements.py", line 21, in should_see_a_link_to
            assert_with_negate(len(world.browser.find_link_by_href(link)) > 0, negate)
          File "/home/bum/Projects/camomile/env_camomile/local/lib/python2.7/site-packages/salad/tests/util.py", line 19, in assert_with_negate
            assert assertion
        AssertionError
    
Что логично, поскольку ссылку на переход на нужную страницу мы еще не добавили. Пойдем в `index.html` и пропишем следующий код внутри тега `{% if user.is_authorized %}`:

    <div class="row-fluid">
    <div class="span6">
    {% blocktrans with nickname_t=user.nickname %}
    <h4 class="muted pull-right">Hello, <span id="nickname-info">{{ nickname_t }}</span></h4>
    {% endblocktrans %}
    </div>
    <div class="span4">
    <a class="btn btn-primary btn-large btn-block" href="/dashboard/">{% trans "Go to Dashboard" %}</a>
    </div>
    </div>

Теперь все сценарии проходят успешно. А вот так выглядит главная страница для залогиненного пользователя:

![](https://dl.dropbox.com/u/381385/devblog/images/making-web-service-04/Selection_005.png)

Итак, сегодня мы создали базовый шаблон с использованием Twitter Bootstrap и научились описывать собственные шаги в сценариях. Вроде бы не так и много, но уже в нескольких следующих частях вы увидите насколько эти инструменты мощны и насколько на самом деле облегчают разработку и сопровождение проектов. Stay tuned!
