---
title: 'Установка nginx на Ubuntu Server 12.04 LTS'
author: 'Сонич Николай'
layout: post
tags: ['ubuntu', 'nginx']
date: '2013-08-30'
---

Вообще в Ubuntu можно довольно таки просто и быстро установить nginx.

    sudo apt-get install nginx

Но, установлена будет далеко не последняя стабильная версия (у меня ставилась 1.1.19, при актуально 1.4.1).
Далее написана инструкция, как установить последнюю стабильную версию nginx в Ubuntu без компиляции исходного кода.

<!-- Read more -->

Сперва обновим локальный список пакетов:

    sudo apt-get update

Установим пакет python-software-properties package (он позволяет легко управлять вашими пакетами и зависимостями):

    sudo apt-get install python-software-properties

Добавляем nginx репозиторий:

    sudo add-apt-repository ppa:nginx/stable

> Для любителей самых свежих версий `ppa:nginx/development`

Добавляем репозиторий в файл sources.list следующей командой:

    echo "deb http://ppa.launchpad.net/nginx/release/ubuntu $(lsb_release -cs) main" >> /etc/apt/sources.list

> для девелоперских версий - `echo "deb http://ppa.launchpad.net/nginx/development/ubuntu $(lsb_release -cs) main" >> /etc/apt/sources.list`

Получаем и добавлем публичный ключ репозитория:

    sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys C300EE8C

Обновляем список пакетов:

    sudo apt-get update

Ствим nginx:

    sudo apt-get install nginx

> Или при необходимости более полную версию `sudo apt-get install nginx-full`

Что бы проверить что мы наставили надо выполнить команду:

    nginx -v

Более детально по nginx можно почитать здесь:

- [официальная документация](http://nginx.org/ru/docs/)
- [вики nginx](http://wiki.nginx.org/Main)
- [Linode Library](https://library.linode.com/web-servers/nginx)
