---
title: 'Скринкаст о RethinkDB'
author: 'Олег Корх'
layout: post
tags: ['rethinkdb','nosql']
date: '2013-01-19'
---

Интересный скринкаст от авторов [RethinkDB](http://www.rethinkdb.com/) об особенностях этой NoSQL базы данных.

<iframe width="640" height="360" src="http://www.youtube.com/embed/cnpSi9qI02E?feature=player_embedded" frameborder="0" allowfullscreen></iframe>

Раскрываются нюансы языка запросов, шардинга и репликации. Вообще, довольно интересный продукт.
