---
title: 'Расширения Markdown в Docpad'
author: 'Олег Корх'
layout: post
tags: ['docpad','coffeescript','static']
date: '2013-01-11'
---

Для [Docpad][docpad] существует очень полезный плагин [docpad-plugin-robotskirt](https://github.com/docpad/docpad-plugin-robotskirt/) позволяющий заметно расширить функциональность разметки [Markdown][markdown] использующейся для набора документов. Просто добавляем следующую строку в файл `package.json` в раздел `dependencies`:

    "docpad-plugin-robotskirt": "2.x",
    "highlight.js": "x.x"

И запускаем `npm install` для установки.

Далее в файле `docpad.coffee` добавляем настройки:

    plugins:
        robotskirt:
            robotskirtOptions:
                EXT_AUTOLINK: true
                EXT_FENCED_CODE: true
                EXT_LAX_SPACING: true
                EXT_NO_INTRA_EMPHASIS: true
                EXT_SPACE_HEADERS: true
                EXT_STRIKETHROUGH: true
                EXT_SUPERSCRIPT: true
                EXT_TABLES: true
                HTML_SKIP_HTML: false
                HTML_SKIP_STYLE: false
                HTML_SKIP_IMAGES: false
                HTML_SKIP_LINKS: false
                HTML_EXPAND_TABS: false
                HTML_SAFELINK: false
                HTML_TOC: false
                HTML_HARD_WRAP: false
                HTML_USE_XHTML: true
                HTML_ESCAPE: false
            smartypants: true
            highlight: (code, lang) ->
                hl = require 'highlight.js'
                has = lang && hl.LANGUAGES.hasOwnProperty(lang.trim())

                open = if has then '<pre><code class="lang-'+lang.trim()+'">' else '<pre><code>'
                body = if has then hl.highlight(lang, code).value else hl.highlightAuto(code).value
                close = '</code></pre>'

                return open + body + close

За один заход мы также устанвливаем потрясающую библиотеку Ивана Сагалаева [hightlight.js](https://github.com/isagalaev/highlight.js), которая умеет автоматически определять язык в блоках кода и подсвечивать синтаксис. Конечно же еще нужно скачать из репозитория данной библиотеки css файл с темой оформления и подключить ее в базовом шаблоне блога.

Вот и все. Теперь на блоге есть подсветка синтаксиса, типограф (например три дефиса заменяются на длинное тире и т.д.), а также ряд дополнительных расширений.

[docpad]:http://docpad.org/
[markdown]:http://daringfireball.net/projects/markdown/syntax


