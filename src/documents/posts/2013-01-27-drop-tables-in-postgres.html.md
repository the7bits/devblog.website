---
title: 'Удаляем таблицы из БД PostgreSQL'
author: 'Олег Корх'
layout: post
tags: ['postgresql', 'sql']
date: '2013-01-27'
---

Часто возникает задача когда необходимо удалить все таблицы из базы данных. Обычно это требуется перед заливкой дампа БД или в различных ситуациях на этапе разработки. Этот небольшой SQL код для PostgreSQL позволит сгенерировать код который в свою очередь уже можно будет использовать для очистки БД.

    select 'drop table if exists "' || tablename || '" cascade;' 
        from pg_tables
        where schemaname = 'public';
        
Подсмотрено на [StackOverflow.com](http://stackoverflow.com/questions/3327312/drop-all-tables-in-postgresql).