---
title: 'Делаем веб-сервис. Часть 3'
author: 'Олег Корх'
layout: post
tags: ['django', 'python', 'service', 'part_3']
date: '2013-02-04'
---

В [предыдущей части](/posts/2013-02-03-making-web-service-02) мы использовали новые возможности Django 1.5 по кастомизации системы аутентификации. В результате ми создали новую модель пользователя `Customer`, настроили админку и написали несколько форм. Теперь пора приступать к реализации основной части сайта. Но мы все же не начнем сразу писать функционал, а сделаем еще кое-что.

Сначала установим библиотеку [django-extensions](https://github.com/django-extensions/django-extensions). Вообще есть смысл включать ее во все проекты, она заметно расширяет возможности скрипта `manage.py` добавляя много полезных функций. Желающие могут ознакомится с функционалом просмотрев [этот скринкаст](http://ericholscher.com/blog/2008/sep/12/screencast-django-command-extensions/).

    $ pip install django-extensions

<!-- Read more -->

Все знают, что при разработке программных продуктов очень важным является тестирование. Существуют разные подходы к реализации автоматизированного тестирования но мы будем использовать подход [BDD](http://en.wikipedia.org/wiki/Behavior-driven_development). BDD (Behavior-driven development) --- это подход при котором сценарии тестирования оформляются в виде спецификаций на специальном языке ([Gherkin](http://www.odr.lu/node/128)), который представляет собой фактически обычный английский язык но с определенными четко определенными конструкциями. При этом такие сценарии пишутся до реализации функциональности (как в [TDD](http://en.wikipedia.org/wiki/Test-driven_development)), а позже используются заодно в роли приемочных тестов. Таким образом тесты становятся неотъемлемой частью процесса разработки.

Итак, займемся интеграцией BDD стека в наш проект. Нам понадобятся следующие библиотеки:

* [Lettuce](http://lettuce.it/) --- главная библиотека для использования BDD,  аналог [Cucumber](http://cukes.info/) из мира Ruby.
* [Splinter](http://splinter.cobrateam.info/) --- библиотека для автоматизации тестирования в браузере, использует Selenium и WebDriver.
* [Salad](https://github.com/wieden-kennedy/salad) --- набор пакетов позволяющий сразу же начинать использовать BDD сценарии без лишней настройки.

Установим их:

    $ pip install git+https://github.com/skoczen/lettuce.git#egg=lettuce
    $ pip install splinter
    $ pip install salad
    
Здесь устанавливается форк Lettuce, поскольку иначе Salad испытывает проблемы с интеграцией с Django. Также добавляем `lettuce.django` в `INSTALLED_APPS` и еще несколько настроек:

``` python
SOUTH_TESTS_MIGRATE = False

LETTUCE_APPS = (
    'accounts',
)
```

Здесь первая настройка говорит South, что для создания тестовой базы данных необходимо использовать `syncdb`, а не пошаговый запуск миграций. Это позволяет увеличить скорость генерации тестовой БД и исключить возможные ошибки при большом количестве миграций. Вторая настройка --- указывает для каких из приложений мы будем использовать тестовые сценарии. Пока в нашем проекте подобное приложение лишь одно, поэтому его и пишем.

Также необходимо в папке содержащей `manage.py` создать файл `terrain.py` который описывает тестовое окружение.

``` python
# coding: utf-8
from salad.terrains.common import *
from salad.terrains.browser import *
from salad.terrains.djangoify import *
from lettuce import before
from salad.logger import logger
from splinter.browser import Browser
```

Попробуем теперь запустить процесс тестирования, для чего используется следующая команда:

    $ ./manage.py harvest
    
В результате мы получим следующее сообщение:

    Django's builtin server is running at 0.0.0.0:8000
    Oops!
    could not find features at ./accounts/features
    
Итак, мы убедились, что установленные библиотеки для тестирования успешно установлены, а сообщение об ошибке говорит о том, что Lettuce не может найти сценарии для приложения `accounts`.

Попробуем описать первый сценарий для приложения `accounts` который будет отвечать за функцию логина. На данный момент созданный пользователь может залогиниться в нашем проекте лишь попытавшись зайти в админку. Сейчас мы пройдем по ключевым этапам разработки в стиле BDD и создадим функцию логина на основном сайте.

Создадим папку `accounts/features`. Теперь внутри данной папки создадим файл `login.feature`, который будет описывать сценарии тестирования функции логина. Обычно на каждую крупную фичу создается отдельный файл, в то же время один файл может содержать столько сценариев сколько будет необходимо для всестороннего тестирования функционала. Запишем в созданный нами файл следующее:

```
Feature: User login
    In order to authenticate the given user
    As a user
    I want to use my email and password for logging in

    Scenario: Log in through the form at the main site
        Given I visit the django url "/accounts/login/"
        When I fill in the field named "username" with "admin@testmail.com"
        And I fill in the field named "password" with "123456"
        And I click on the button named "submit"
        Then I should see that the element with id "nickname-info" contains exactly "admin"
```        
        
Вот такой простенький сценарий, смысл которого я думаю абсолютно понятен просто из его содержания. Самое крутое в BDD --- это то, что при определенном обучении такие сценарии может писать не только программист, но и например менеджер. Но к этой теме я наверное как-то еще вернусь в будущих статьях. Итак, попробуем запустить команду `./manage.py harvest` и посмотреть, что будет.

```
$ ./manage.py harvest
Django's builtin server is running at 0.0.0.0:8000

Feature: User login                                                                     # accounts/features/login.feature:1
  In order to authenticate the given user                                               # accounts/features/login.feature:2
  As a user                                                                             # accounts/features/login.feature:3
  I want to use my email and password for logging in                                    # accounts/features/login.feature:4

  Scenario: Log in through the form at the main site                                    # accounts/features/login.feature:6
    Given I visit the django url "/accounts/login/"                                     # accounts/features/login.feature:7
    When I fill in the field named "username" with "admin@testmail.com"                 # accounts/features/login.feature:8
    And I fill in the field named "password" with "123456"                              # accounts/features/login.feature:9
    And I click on the button named "submit"                                            # accounts/features/login.feature:10
    Then I should see that the element with id "nickname-info" contains exactly "admin" # accounts/features/login.feature:11

1 feature (0 passed)
1 scenario (0 passed)
5 steps (5 undefined, 0 passed)

You can implement step definitions for undefined steps with these snippets:

# -*- coding: utf-8 -*-
from lettuce import step

@step(u'Given I visit the django url "([^"]*)"')
def given_i_visit_the_django_url_group1(step, group1):
    assert False, 'This step must be implemented'
@step(u'When I fill in the field named "([^"]*)" with "([^"]*)"')
def when_i_fill_in_the_field_named_group1_with_group2(step, group1, group2):
    assert False, 'This step must be implemented'
@step(u'And I fill in the field named "([^"]*)" with "([^"]*)"')
def and_i_fill_in_the_field_named_group1_with_group2(step, group1, group2):
    assert False, 'This step must be implemented'
@step(u'And I click on the button named "([^"]*)"')
def and_i_click_on_the_button_named_group1(step, group1):
    assert False, 'This step must be implemented'
@step(u'Then I should see that the element with id "([^"]*)" contains exactly "([^"]*)"')
def then_i_should_see_that_the_element_with_id_group1_contains_exactly_group2(step, group1, group2):
    assert False, 'This step must be implemented'
(finished within 5 seconds)
```

Пока ничего особенного, Lettuce вывел сообщение, что он не понимает наш сценарий и предлагает описать шаги. На самом деле, вышеприведенный сценарий использует стандартные фразы которые уже описаны библиотекой Salad, просто пока Lettuce их не видит. Создадим здесь же еще один файл, и назвем его `common_steps.py`:

``` python
# coding: utf-8
from salad.steps.everything import *
```

И попробуем вновь запустить прогон сценариев. Результат теперь будет кардинально отличаться:

```
$ ./manage.py harvest
Django's builtin server is running at 0.0.0.0:8000

Feature: User login                                                                     # accounts/features/login.feature:1
  In order to authenticate the given user                                               # accounts/features/login.feature:2
  As a user                                                                             # accounts/features/login.feature:3
  I want to use my email and password for logging in                                    # accounts/features/login.feature:4

  Scenario: Log in through the form at the main site                                    # accounts/features/login.feature:6
    Given I visit the django url "/accounts/login/"                                     # home/bum/P    Given I visit the django url "/accounts/login/"                                     # home/bum/Projects/camomile/env_camomile/local/lib/python2.7/site-packages/salad/steps/djangoify.py:8
    Traceback (most recent call last):
      File "/home/bum/Projects/camomile/env_camomile/local/lib/python2.7/site-packages/lettuce/core.py", line 143, in __call__
        ret = self.function(self.step, *args, **kw)
      File "/home/bum/Projects/camomile/env_camomile/local/lib/python2.7/site-packages/salad/steps/djangoify.py", line 9, in go_to_the_url
        world.response = world.browser.visit(django_url(url))
      File "/home/bum/Projects/camomile/env_camomile/local/lib/python2.7/site-packages/splinter/driver/webdriver/__init__.py", line 45, in visit
        self.ensure_success_response()
      File "/home/bum/Projects/camomile/env_camomile/local/lib/python2.7/site-packages/splinter/request_handler/request_handler.py", line 31, in ensure_success_response
        self.status_code.is_valid_response()
      File "/home/bum/Projects/camomile/env_camomile/local/lib/python2.7/site-packages/splinter/request_handler/status_code.py", line 54, in is_valid_response
        raise HttpResponseError(self.code, self.reason)
    HttpResponseError: 404 - Not Found
    When I fill in the field named "username" with "admin@testmail.com"                 # home/bum/P    When I fill in the field named "username" with "admin@testmail.com"                 # home/bum/Projects/camomile/env_camomile/local/lib/python2.7/site-packages/salad/steps/browser/forms.py:14
    And I fill in the field named "password" with "123456"                              # home/bum/P    And I fill in the field named "password" with "123456"                              # home/bum/Projects/camomile/env_camomile/local/lib/python2.7/site-packages/salad/steps/browser/forms.py:14
    And I click on the button named "submit"                                            # home/bum/P    And I click on the button named "submit"                                            # home/bum/Projects/camomile/env_camomile/local/lib/python2.7/site-packages/salad/steps/browser/mouse.py:49
    Then I should see that the element with id "nickname-info" contains exactly "admin" # home/bum/P    Then I should see that the element with id "nickname-info" contains exactly "admin" # home/bum/Projects/camomile/env_camomile/local/lib/python2.7/site-packages/salad/steps/browser/elements.py:48

1 feature (0 passed)
1 scenario (0 passed)
5 steps (1 failed, 4 skipped, 0 passed)
(finished within 6 seconds)

```

Как видим, сценарий начал выполняться, но вылетел с ошибкой на первом же шаге. Что логично, у нас нет еще страницы `/accounts/login/`. Теперь приступаем к реализации функциональности. При этом обратите внимание, что сценарий одновременно является одновременно и документацией (программист прочитав его понимает, что же ему нужно реализовать), и приемочным тестом.  Когда Lettuce отрапортует об успешном прохождении сценария --- это будет означать, что функциональность реализована. Итак, теперь приступим собственно к реализации функционала.

Первым делом добавим в `urlpatterns` в главном `urls.py` следующее:

``` python
url(r'^accounts/', include('accounts.urls')),
```

Таким образом, мы перенаправили все ссылки начинающиеся с `/accounts/` на обработку в приложение `accounts`. Теперь, создадим файл `accounts/urls.py`.

``` python
# coding: utf-8
from django.conf.urls.defaults import patterns, include, url
from django.contrib.auth.views import login

urlpatterns = patterns('',
    url(r'^login/$', login),
)
```
   
В данном случае нам не нужно реализовывать самим функцию логина, поскольку авторы Django уже предусмотрели стандартное отображение `django.contrib.auth.views.login`. Попробуем запустить тестовый сценарий еще раз. Конечно, же мы снова увидим сообщение об ошибке (я не копирую полную выдачу, ради экономии места):

    HttpResponseError: 500 - Internal Server Error
    
Мало информативно, правда? Здесь есть один нюанс, при прогоне тестов Django отключает отладочный режим, чтобы тестовое окружение максимально напоминало продакшн. Конечно, можно запустить отладочный сервер, зайти с помощью браузера на нужную нам страницу и посмотреть стектрейс, а можно еще раз запустить прогон сценария но указав параметр `-d`, что включит отладочный режим. Теперь в выдаче мы увидим следующее:

        raise TemplateDoesNotExist(name)
    TemplateDoesNotExist: registration/login.html
    
Теперь все становится ясно, мы не подготовили шаблон, в который будет вставляться форма логина. Создадим файл `accounts/templates/accounts/login.html`:

    {% load i18n %}
    <form method="post">
    {% csrf_token %}
    {{ form.as_ul }}
    <input type="submit" name="submit" value="{% trans 'Login' %}" />
    <input type="hidden" name="next" value="{{ next }}" />
    </form>
    
Пока вообще не думаем о внешнем виде, им займемся позже, поэтому шаблон довольно прост. Далее вносим коррективы в `accounts/urls.py`:

``` python
url(r'^login/$', login, {'template_name': 'accounts/login.html'}),
```

Теперь, при попытке очередного прогона мы сможем увидеть как открывается окно браузера, в нем автоматически заполняется форма логина и происходит попытка входа. Правда сценарий завершается с ошибкой на последнем шаге, но есть и еще одна проблема --- пока браузер не успел закрыться, мы видим сообщение о том, что указанной нами пары электронная почта/пароль не существует. Более того, если просмотреть базу данных с помощью pgAdmin, то можно заметить, что в БД пропали записи пользователей. Дело в том, что для прогона тестов мы используем базу данных `default`, а Salad сбрасывает БД перед запуском каждого сценария.

В идеале конечно нужно иметь отдельно основную, а отдельно тестовую базу данных. Но во время активной разработки поддерживать две БД довольно накладно, поэтому пойдем немного другим путем --- создадим команду для `manage.py` которая будет автоматически инициализировать БД набором данных. При этом для данной команды реализуем параметр `test_mode` в случае указания которого БД будет инициализироваться расширенным набором данных.

Сервисные команды привязываются к приложениям, поэтому создадим еще одно приложение `core`:

    $ ./manage.py startapp core

Создадим теперь внутри папки `core` подпапку `management`, в нем создадим файл `__init__.py` и еще одну папку `commands`. Внутри нее создадим еще файлы `__init__.py` и `initialize.py`. Внутри последнего опишем нашу новую команду:

``` python
# coding: utf-8
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    args = ''
    help = 'Initializes Camomile'

    def handle(self, *args, **options):
        from django.contrib.auth import get_user_model

        customer = get_user_model()
        u_admin = customer.objects.create_user(
                email='admin@testmail.com',
                nickname='admin',
                password='123456',
                )
        u_admin.is_admin = True
        u_admin.is_superuser = True
        u_admin.activation_code = 'ALREADY_ACTIVATED'
        u_admin.save()

        if 'test_mode' in args:
            for i in range(1, 10):
                u = customer.objects.create_user('user%d@testmail.com'%i, 'user%d'%i, '123456')
                u.activation_code = 'ALREADY_ACTIVATED'
                u.save()
```

Теперь мы можем без проблем наполнить БД необходимыми данными. Чтобы перед прогонкой каждого сценария БД инициализировалась необходимыми данными, допишем соответствующий обработчик в `terrain.py`:

``` python
@before.each_scenario
def init_data(scenario):
    '''
    Init data before each scenario
    '''
    call_command('initialize', 'test_mode', verbosity=0)
```

Если попробовать прогнать сценарий теперь, то пользователь успешно залогинится, но потом мы увидим в отчете следующую ошибку:

    ElementDoesNotExist

Теперь нам нужно реализовать главную страницу и предусмотреть там вывод ника залогиненного пользователя согласно тому как это описывается в сценарии.

Создадим базовый шаблон `camomile/templates/base.html`:

```
{% load i18n %}
<!doctype html>
<html>
<head>
    <title>{% block title %}Camomile{% endblock %}</title>
</head>
<body>
    {% if user.is_authenticated %}
    <span id="nickname-info">{{ user.nickname }}</span>
    {% else %}
    <a href="/accounts/login/">{% trans "Login" %}</a>
    {% endif %}

{% block content %}{% endblock %}
</body>
</html>
```

И еще один --- `index.html`:

```
{% extends "base.html" %}
```

Далее, идем в `urls.py` и пишем следующее:

``` python
from django.views.generic import TemplateView

...
    (r'^$', TemplateView.as_view(template_name="index.html")),
...
```

Здесь мы использовали стандартный класс `TemplateView` для создания простой страницы с шаблоном. Также, чтобы после логина пользователь перенаправлялся на главную страницу, необходимо указать в `settings.py` следующую настройку:

    LOGIN_REDIRECT_URL = '/'
    
И вот теперь наконец после очередного прогона сценария мы увидим следующее:

```
$ ./manage.py harvest
Django's builtin server is running at 0.0.0.0:8000

Feature: User login                                                                     # accounts/features/login.feature:1
  In order to authenticate the given user                                               # accounts/features/login.feature:2
  As a user                                                                             # accounts/features/login.feature:3
  I want to use my email and password for logging in                                    # accounts/features/login.feature:4

  Scenario: Log in through the form at the main site                                    # accounts/features/login.feature:6
    Given I visit the django url "/accounts/login/"                                     # home/bum/P    Given I visit the django url "/accounts/login/"                                     # home/bum/Projects/camomile/env_camomile/local/lib/python2.7/site-packages/salad/steps/djangoify.py:8
    When I fill in the field named "username" with "admin@testmail.com"                 # home/bum/P    When I fill in the field named "username" with "admin@testmail.com"                 # home/bum/Projects/camomile/env_camomile/local/lib/python2.7/site-packages/salad/steps/browser/forms.py:14
    And I fill in the field named "password" with "123456"                              # home/bum/P    And I fill in the field named "password" with "123456"                              # home/bum/Projects/camomile/env_camomile/local/lib/python2.7/site-packages/salad/steps/browser/forms.py:14
    And I click on the button named "submit"                                            # home/bum/P    And I click on the button named "submit"                                            # home/bum/Projects/camomile/env_camomile/local/lib/python2.7/site-packages/salad/steps/browser/mouse.py:49
    Then I should see that the element with id "nickname-info" contains exactly "admin" # home/bum/P    Then I should see that the element with id "nickname-info" contains exactly "admin" # home/bum/Projects/camomile/env_camomile/local/lib/python2.7/site-packages/salad/steps/browser/elements.py:48

1 feature (1 passed)
1 scenario (1 passed)
5 steps (5 passed)
(finished within 7 seconds)
```

Сценарий полностью пройден, а значит функционал успешно реализован. Вот приблизительно так и работает BDD. Конечно дальше у нас будет намного больше сценариев, пока главным было показать принцип и интегрировать стек тестирования в проект. На этом заканчиваю третью часть, скоро продолжим.