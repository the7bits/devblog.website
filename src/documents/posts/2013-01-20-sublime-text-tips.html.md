---
title: 'Маленькие хитрости Sublime Text 2'
author: 'Олег Корх'
layout: post
tags: ['sublimetext','editor']
date: '2013-01-20'
---

Мой основной текстовый редактор - это [Sublime Text 2](http://www.sublimetext.com/). Этот потрясающий редактор покоряет своим удобством, функциональностью и настраиваемостью всего и вся. 

![](https://dl.dropbox.com/u/381385/devblog/images/20130120/sublime.png)

Вот несколько настроек подсмотренных в [блоге у Wes Bos](http://wesbos.com/category/sublime-text/). Настройки прописываются в конфигурационном файле который открывается через меню `Preferences -> Settings - User`.

* `"highlight_modified_tabs": true` --- подсветка вкладки с модифицированным текстом.
* `"caret_style": "phase"` --- плавное моргание курсора с затухающим эффектом.
* `"highlight_line": true` --- подсветка текущей строки.
* Следующие настройки позволяют добавить по 1 пикселю сверху и снизу строки, что несного разрежает код и улучшает читаемость:

        "line_padding_bottom": 1,
        "line_padding_top": 1
        
* `"fade_fold_buttons": false` --- показать кнопки сворачивания кода.
* `"bold_folder_labels": true` --- отображение папок в боковой панели жирным шрифтом.
