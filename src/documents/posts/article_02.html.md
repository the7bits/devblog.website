---
title: 'Автообновление блога и Webfaction'
author: 'Олег Корх'
layout: post
tags: ['docpad','coffeescript','static','webfaction']
date: '2013-01-10'
---

Во время разработки данного блога всплыла еще одна проблема, которая довольно часто всплывает и при разработке других проектов. А именно, как максимально автоматизировать процесс обновления файлов на сервере, при этом не слишком все усложняя? Попробуем выработать определенный механизм.

Итак, во первых в `.gitignore` добавляем папку `out` и соотвественно делаем `git rm --cached -r out` чтобы удалить ее из репозитория. Смысл в том, чтобы оставить в репозитории лишь исходные коды проекта, а генерацию сайта как такового переложить на сервер. Уточню, что все исхожные коды данного блога находятся в открытом доступе на [Bitbucket'е](https://bitbucket.org/the7bits/devblog.website).

Первым делом настроим отсылку нотификаций о новых коммитах от Bitbucket'а. Различных вариантов интеграции с другими сервисами Bitbucket предлагает много, но мы выберем самый простой --- электронную почту. Итак, заходим в настройки проекта в раздел Services, выбираем Email и нажимаем на кнопку Add service. Далее нужно ввести адрес электронной почты, в нашем случае это `devblog-upgrade@the7bits.com`. Теперь, при каждом новом коммите, Bitbucket будет отсылать новое сообщение по данному адресу.

<!-- Read more -->

Следующим этапом настроим Webfaction таким образом, что бы при получении сообщения на данный адрес на сервере запускался бы определенный скрипт. Пусть это будет `devblog_upgrade.py` в корне домашней папки. В данном случае мы используем Python поскольку на нем писать различные сервисные скрипты проще чем на Bash.

![](https://dl.dropbox.com/u/381385/devblog/images/article_02/image_01.png)

Давайте теперь посмотрим, какая информация пересылается в письме. Логинимся по SSH на сервер и создаем файл `devblog_upgrade.py` следующего содержания:

    #!/usr/bin/env python

    import sys

    output = open('/home/drom/devblog-script.out', 'a')
    output.write(sys.stdin.read())
    output.close()

Данный скрипт делает одну единственную вещь - зписывает все, что было передано ему через стандартный поток ввода в текстовый файл. Так мы сможем проверить, какая именно информация нам пришла. Также необходимо сделать этот файл запускаемым:

    $ chmod u+x devblog_upgrade.py

Теперь можно попробовать сделать коммит в репозиторий. Довольно быстро в папке пользователя на сервере появился новый файл `devblog-script.out` со следующим содержанием:

    From commits-noreply@bitbucket.org  Thu Jan 10 01:12:50 2013
    Return-Path: <commits-noreply@bitbucket.org>
    X-Original-To: script-820826e2d968f47994bbde6ccc19d560b6a8a3e3@mx9.webfaction.com
    Delivered-To: script-820826e2d968f47994bbde6ccc19d560b6a8a3e3@mx9.webfaction.com
    Received: from localhost (localhost.localdomain [127.0.0.1])
        by mx9.webfaction.com (Postfix) with ESMTP id 1DD2551283FF
        for <script-820826e2d968f47994bbde6ccc19d560b6a8a3e3@mx9.webfaction.com>; Thu, 10 Jan 2013 01:12:50 -0600 (CST)
    X-Spam-Flag: NO
    X-Spam-Score: -1.9
    X-Spam-Level: 
    X-Spam-Status: No, score=-1.9 tagged_above=-999 required=3
        tests=[BAYES_00=-1.9]
    Received: from mx9.webfaction.com ([127.0.0.1])
        by localhost (mail9.webfaction.com [127.0.0.1]) (amavisd-new, port 10024)
        with ESMTP id A4Qmw20mU5tf
        for <script-820826e2d968f47994bbde6ccc19d560b6a8a3e3@mx9.webfaction.com>;
        Thu, 10 Jan 2013 01:12:48 -0600 (CST)
    Received: from bitbucket10.managed.contegix.com (bitbucket10.managed.contegix.com [207.223.240.188])
        by mx9.webfaction.com (Postfix) with ESMTP id A12F451280F3
        for <devblog-upgrade@the7bits.com>; Thu, 10 Jan 2013 01:12:48 -0600 (CST)
    Received: from bitbucket02.private.bitbucket.org (bitbucket02.private.bitbucket.org [172.16.10.2])
        by bitbucket10.managed.contegix.com (Postfix) with ESMTP id AC8C614150B
        for <devblog-upgrade@the7bits.com>; Thu, 10 Jan 2013 01:12:47 -0600 (CST)
    Received: from bitbucket02.managed.contegix.com (bitbucket02.managed.contegix.com [127.0.0.1])
        by bitbucket02.private.bitbucket.org (Postfix) with ESMTP id B35B3240DD5
        for <devblog-upgrade@the7bits.com>; Thu, 10 Jan 2013 01:12:47 -0600 (CST)
    Content-Type: text/plain; charset="utf-8"
    MIME-Version: 1.0
    Content-Transfer-Encoding: quoted-printable
    Subject: commit/devblog.website: korkholeh: check
    From: Bitbucket <commits-noreply@bitbucket.org>
    To: devblog-upgrade@the7bits.com
    Date: Thu, 10 Jan 2013 07:12:47 -0000
    Message-ID: <20130110071247.13279.78930@bitbucket02.managed.contegix.com>

    1 new commit in devblog.website:

    https://bitbucket.org/the7bits/devblog.website/commits/d2037a28ad36/
    changeset:   d2037a28ad36
    branch:      master
    user:        korkholeh
    date:        2013-01-10 08:12:30
    summary:     check
    affected #:  2 files

    Repository URL: https://bitbucket.org/the7bits/devblog.website/

    --

    This is a commit notification from bitbucket.org. You are receiving
    this because you have the service enabled, addressing the recipient of
    this email.

Проанализировав текст сообщения можно написать уже полноценный скрипт:

``` python
#!/usr/bin/env python
import sys
import os
import email
import subprocess

COMMANDS = [
    'git pull',
    'npm install',
    'docpad generate --env static',
    'rm -rf /home/drom/webapps/7b_devblog/*',
    'cp -rf out/* /home/drom/webapps/7b_devblog/',
    'chmod 755 -R /home/drom/webapps/7b_devblog/*',
]

s = sys.stdin.read()

output = open('/home/drom/devblog-script.out', 'a')
output.write(s)
output.close()

msg = email.message_from_string(s)

if msg['From'] == 'Bitbucket <commits-noreply@bitbucket.org>':
    body=''
    for part in msg.walk():
        if part.get_content_type():
            body += part.get_payload(decode=True)
    data = dict(
        filter(
            lambda item: len(item) == 2,
            map(
                lambda x:map(
                    lambda t:t.strip(), 
                    x.split(':')
                    ), 
                body.split('\n')
                )
            )
        )
    if data['branch'] == 'master' and data ['summary'] == 'publish':
        os.chdir('/home/drom/temp/devblog/devblog.website')
        for cmd in COMMANDS:
            subprocess.call(cmd, shell=True)
```

Готово. Теперь публикация статей на блог выглядит проще простого - достачно просто закоммитить изменения в ветку `master`, указать в качестве комментария коммита слово `publish` и сделать `git push origin master`. Вот и все.
    

