---
title: 'Пишем блогодвижок на Docpad'
author: 'Олег Корх'
layout: post
tags: ['docpad','coffeescript','static']
date: '2013-01-09'
---

Начать вести данный блог мне хотелось бы не совсем стандартно, а именно с описания технологий которые использовались для его создания. Последнее время, все больше набирают популярность генераторы статических сайтов. Вот и здесь мы попробуем использовать один из таких движков, а именно [Docpad][docpad]. Он достаточно функционален и в то же время не сложен, использует для работы [Node.js][nodejs], поэтому процесс установки также довольно прост.

Итак, первым делом создадим Git репозиторий для хранения исходных кодов нашего нового блога. Для хостинга исходных кодов будем использовать [Bitbucket][bitbucket]. Создаем новый репозиторий:

![](https://dl.dropbox.com/u/381385/devblog/images/article_01/image_01.png)

<!-- Read more -->

Теперь идем в папку с проектами на нашей локальной машине, создаем там папку `devblog.website`, инициализируем репозиторий и подключаем внешний источник.

``` bash
$ mkdir devblog.website
$ cd devblog.website
$ git init
$ git remote add origin ssh://git@bitbucket.org/the7bits/devblog.website.git
```

Таким образом мы создали рабочую папку и настроили Git для работы с исходными текстами. Следующим этапом устанавливаем [Docpad][docpad] (естественно до этого у нас уже должен быть установлен [Node.js][nodejs]):

``` bash
$ sudo npm install -fg docpad@6.21
```
        
Теперь создадим следующую структуру папок:

    src /
        documents /
            scripts
            styles
        files /
            vendor
        layouts
        
Предназначение данных папок следующее:

* `documents` - содержит файлы с документами (статьи в блоге, статические страницы)
* `documents/scripts` - клиентские скрипты
* `documents/styles` - файлы с описанием стилей
* `files` - статические сайты
* `files/vendor` - содержит файлы сторонних библиотек
* `layouts` - шаблоны описывающие верстку страниц

Итак продолжим, первым делом идем на сайт [Twitter Boostrap][bootstrap], скачиваем последний релиз и распаковываем архив в папку `files/vendor`. Далее скачиваем [jQuery][jquery] и также кладем файл `jquery.js` в папку `files/vendor`.

В папке `documents/styles` создадим новый файл `styles.css.styl` в который будем писать кастомные стили нашего сайта, пока запишем туда следующее:

    body
        padding-top 20px
        padding-bottom 40px

    /* Custom container */
    .container-narrow
        margin 0 auto
        max-width 700px

    .container-narrow > hr
        margin 30px 0

В папке `documents/scripts` создадим новый файл `script.js.coffee` в который позже можно будет писать клиентский код на CoffeeScript, но пока в него напишем только следующий код:

``` coffeescript
    $ ->
```

Теперь создадим файл `src/layouts/default.html.eco` который будет являть собой основной шаблон сайта и пока напишем в него следующее:

    <!DOCTYPE html>
    <html lang="ru">
    <head>
        <!-- Meta -->
        <meta charset="utf-8" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    
        <!-- Use the .htaccess and remove these lines to avoid edge case issues.
           More info: h5bp.com/i/378 -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    
        <!-- Our site title and description -->
        <title><%= @getPreparedTitle() %></title>
        <meta name="description" content="<%= @getPreparedDescription() %>" />
        <meta name="keywords" content="<%= @getPreparedKeywords() %>" />
        <meta name="author" content="<%= @site.author or '' %>" />
    
        <!-- Output DocPad produced meta elements -->
        <%- @getBlock('meta').toHTML() %>
    
        <!-- Mobile viewport optimized: h5bp.com/viewport -->
        <meta name="viewport" content="width=device-width" />
    
        <!-- DocPad Meta Information -->
        <%- @getBlock('meta').toHTML() %>
       
        <!-- Shims: IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script async src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    
        <!-- Styles -->
        <%- @getBlock('styles').add(["/vendor/bootstrap/css/bootstrap.css","/vendor/bootstrap/css/bootstrap-responsive.css","/styles/styles.css"]).toHTML() %>
    </head>
    <body>
    <div class="container-narrow">
    
        <div class="masthead">
            <ul class="nav nav-pills pull-right">
            <li <% if @document.slug!='contacts': %>class="active"<% end %> >
            <a href="/">Блог</a></li>
            <li <% if @document.slug=='contacts': %>class="active"<% end %> >
            <a href="/contacts">Контакты</a></li>
                <li><a href="/atom.xml">RSS</a></li>
            </ul>
            <h3 class="muted">Блог разработчиков 7bits</h3>
          </div>
    
          <hr>
    
        <div class="row-fluid">
            <div class="span12">
                <%- @content %>
            </div>
        </div>
    
        <hr>
    
        <div class="footer">
            <p>&copy; 7bits 2013</p>
        </div>
    
        </div> <!-- /container -->
        <!-- Scripts -->
        <%- @getBlock('scripts').add(["/vendor/jquery.js","/vendor/bootstrap/js/bootstrap.min.js","/scripts/script.js"]).toHTML() %>
    </body>
    </html>
    
Выше мы создали шаблон основной страницы используя шаблонизатор [Eco][eco], а теперь нужно создать собственно саму страницу. Для этого создадим файл `documents/index.html.eco`:

``` markdown
---
layout: 'default'
---
```

Еще необходимо создать файл `docpad.coffee` со всеми необходимыми настройками нашего сайта, пока запишем в него следующее:

``` coffeescript
# The DocPad Configuration File
# It is simply a CoffeeScript Object which is parsed by CSON
docpadConfig = {

    # =================================
    # Template Data
    # These are variables that will be accessible via our templates
    # To access one of these within our templates, refer to the FAQ: https://github.com/bevry/docpad/wiki/FAQ

    templateData:

        # Specify some site properties
        site:
            # The production url of our website
            url: "http://devblog.the7bits.com"

            # Here are some old site urls that you would like to redirect from
            oldUrls: [
                'devblog.the7bits.com'
            ]

            # The default title of our website
            title: "Блог разработчиков 7bits"

            # The website description (for SEO)
            description: """
                Блог разработчиков 7bits                
                """

            # The website keywords (for SEO) separated by commas
            keywords: """
                блог, программирование, разработка, 7bits
                """

            # The website author's name
            author: "Oleh Korkh"

            # The website author's email
            email: "oleh.korkh@the7bits.com"


        # -----------------------------
        # Helper Functions

        # Get the prepared site/document title
        # Often we would like to specify particular formatting to our page's title
        # we can apply that formatting here
        getPreparedTitle: ->
            # if we have a document title, then we should use that and suffix the site's title onto it
            if @document.title
                "#{@document.title} | #{@site.title}"
            # if our document does not have it's own title, then we should just use the site's title
            else
                @site.title

        # Get the prepared site/document description
        getPreparedDescription: ->
            # if we have a document description, then we should use that, otherwise use the site's description
            @document.description or @site.description

        # Get the prepared site/document keywords
        getPreparedKeywords: ->
            # Merge the document keywords with the site keywords
            @site.keywords.concat(@document.keywords or []).join(', ')


    # =================================
    # Collections

    # =================================
    # DocPad Events

    # Here we can define handlers for events that DocPad fires
    # You can find a full listing of events on the DocPad Wiki
    events:

        # Server Extend
        # Used to add our own custom routes to the server before the docpad routes are added
        serverExtend: (opts) ->
            # Extract the server from the options
            {server} = opts
            docpad = @docpad

            # As we are now running in an event,
            # ensure we are using the latest copy of the docpad configuraiton
            # and fetch our urls from it
            latestConfig = docpad.getConfig()
            oldUrls = latestConfig.templateData.site.oldUrls or []
            newUrl = latestConfig.templateData.site.url

            # Redirect any requests accessing one of our sites oldUrls to the new site url
            server.use (req,res,next) ->
                if req.headers.host in oldUrls
                    res.redirect(newUrl+req.url, 301)
                else
                    next()
}


# Export our DocPad Configuration
module.exports = docpadConfig
```
        
И на последок создадим в папке проекта файл `package.json` с необходимой информацией о проекте и его зависимостях:

``` javascript
{
    "name": "devblog.website",
    "version": "0.1.0",
    "description": "7bits developer blog",
    "homepage": "https://bitbucket.org/the7bits/devblog.website",
    "keywords": [
        "devblog",
        "website",
        "7bits"
    ],
    "author": "Oleh Korkh <oleh.korkh@the7bits.com> (oleh.korkh@the7bits.com)",
    "maintainers": [
        "Oleh Korkh <oleh.korkh@the7bits.com> (oleh.korkh@the7bits.com)"
    ],
    "contributors": [
        "Oleh Korkh <oleh.korkh@the7bits.com> (oleh.korkh@the7bits.com)"
    ],
    "bugs": {
        "url": "https://bitbucket.org/the7bits/devblog.website/issues?status=new&status=open"
    },
    "repository" : {
        "type" : "git",
        "url" : "git@bitbucket.org:the7bits/devblog.website.git"
    },
    "engines" : {
        "node": "0.8.x",
        "npm": "1.1.x"
    },
    "dependencies": {
        "docpad": "6.x",
        "docpad-plugin-cleanurls": "2.x",
        "docpad-plugin-coffeescript": "2.x",
        "docpad-plugin-eco": "2.x",
        "docpad-plugin-less": "2.x",
        "docpad-plugin-marked": "2.x",
        "docpad-plugin-partials": "2.x",
        "docpad-plugin-stylus": "2.x",
        "docpad-plugin-text": "2.x"
    },
    "devDependencies": {
        "docpad-plugin-livereload": "2.x"
    },
    "main": "node_modules/docpad/bin/docpad-server"
}
```

Теперь устанавливаем все необходимые библиотеки:

    $ npm install
    
Попробуем запустить на исполнение команду `docpad run`, после чего откроем в браузере ссылку http://localhost:9778/ .

![](https://dl.dropbox.com/u/381385/devblog/images/article_01/image_02.png)

Итак, наш сайт заработал. Если еще раз посмотреть в папку с проектом, то можно заметить, что в нем появились две новые папки:

* `node_modules` - сюда были установлены необходимые плагины [Docpad][docpad] и библиотеки [Node.js][nodejs]
* `out` - все файлы сгенерированного сайта

Таким образом мы сумели создать все необходимые файлы для того чтобы сгенерировать главную страницу сайта, но теперь нужно написать собственно блогодвижок. 

Какие самые главные этапы в реализации блога? Конечно же необходимо реализовать страницы постов, а также страницу со списком постов отсортированных по дате публикации (в обратном порядке). Также очень желательно иметь возможность помечать некоторые страницы как черновики, если статья пишется долгое время. Начнем со страниц постов. Первое, что нам понадобится - это описать шаблон такой страницы. Создадим файл `layouts/post.html.eco`:

    ---
    layout: default
    ---
    
    <article id="post" class="post">
        <h1><%= @document.title %></h1>
        <div class="post-content"><%- @content %></div>
    </article>

Обратите внимание, что запись `layout: default` указывает движку, что данный шаблон наследуется от шаблона `layouts/default.html.eco`, весь текст который находится ниже сервисного блока будет подставлен в родительский шаблон вместо переменной `content`.

Хорошо, шаблон для постов мы создали, теперь необходимо написать какую-то статью. Начнем с классического текста [Lorem ipsum][lorem]. Для всех постов в блог создадим папку `documents/posts`, после чего в ней создадим файл `lorem.html.md`. Для набора текста используется язык разметки [Markdown][markdown].

``` markdown
    ---
    title: Lorem ipsum
    layout: post
    ---
    
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
    Phasellus sit amet laoreet sapien. Phasellus luctus blandit 
    risus, non consequat justo adipiscing sodales. Integer nec 
    tincidunt mauris. Maecenas accumsan augue ac neque varius 
    posuere. In sed congue sem. Nulla facilisi. Integer egestas, 
    magna at pellentesque accumsan, neque quam varius nunc, sed 
    molestie lacus dolor at diam. Quisque ultricies aliquam leo, 
    vitae dignissim urna pellentesque ac. Vestibulum viverra 
    venenatis quam, eget dignissim tellus pharetra at. Morbi 
    vehicula ipsum sit amet urna vehicula sodales. Proin ut 
    tortor dui. Nam scelerisque eros enim, sit amet elementum 
    turpis. Nunc sed viverra urna. Vestibulum bibendum semper 
    varius. Nam aliquam mattis lorem.
```

Набираем в браузере http://localhost:9778/posts/lorem и смотрим на результат:

![](https://dl.dropbox.com/u/381385/devblog/images/article_01/image_03.png)

Итак, создавать страницы постов мы уже научились. Создадим еще набор статей с подобным содержанием, специально для тестов. Получим в папке `documents/posts` приблизительно такой набор файлов:

    lorem.html.md
    lorem1.html.md
    lorem2.html.md
    lorem3.html.md
    lorem4.html.md
    lorem5.html.md
    lorem6.html.md
    lorem7.html.md
    lorem8.html.md
    lorem9.html.md
    lorem10.html.md
    
Теперь попробуем немного усложнить отображение постов в блоге. Дополним шапку статьи информацией об авторе и дате публикации. Итак, сделаем шапку нашей статьи приблизительно такой:

    ---
    title: Lorem ipsum
    author: Олег Корх
    layout: post
    date: '2013-01-08'
    ---
    
Вообще, подобным образом можно добавлять как угодно много параметров на каждый документ и позже мы еще к этому вернемся. Теперь скорректируем текст шаблона статьи, в том месте где он отображает заголовок, следующим образом:

    <h1><%= @document.title %></h1>
    <h6><%= @document.author %> | <%= @document.date.toShortDateString() %></h6>
    
Смотрим на страницу и видим, что заголовок дополнился новой информацией.

![](https://dl.dropbox.com/u/381385/devblog/images/article_01/image_04.png)

Хорошо, теперь реализуем в нашем блоге функциональность помогающую искать похожие страницы. Для этого нам понадобится плагин [docpad-plugin-related](https://npmjs.org/package/docpad-plugin-related). Откроем файл `package.json` и в конец раздела `dependencies` добавим еще одну запись:

    "docpad-plugin-related": "2.x"
    
Теперь собственно доустановим недостающий плагин:

    $ npm install
    
Работает плагин `docpad=plugin-related` очень просто. Он анализирует метаданные статьи и ищет там атрибут `tags`, который являет собой массив тегов. Например, он может выглядеть так: `tags: ['post', 'python', 'туториал']`. Используя теги статей плагин для каждого такого документа генерирует атрибут `relatedDocuments` который содержит ссылки на похожие документы. Например если мы хотим показывать внизу каждой страницы со статьей ссылки на три похожие статьи, то просто добавим в соответствующее место шаблона `post.html.eco` следующий код:

    <footer class="article-footer">
        <% if  @document.relatedDocuments and @document.relatedDocuments.length: %>
        <section id="related">
            <h3>Похожие статьи</h3>
            <table class="related-links">
                <tr>
                <% for document in @document.relatedDocuments[0..2]: %>
                    <td><h5><a href="<%= document.url %>"><%= document.title %></a></h5></td>
                <% end %>
                </tr>
            </table>
        </section>
        <% end %>
    </footer>

Добавим также стили в `styles.css.styl`:

    footer.article-footer
        margin-top 30px
    
    table.related-links
        width 100%
    
        tr
            width 33%

Смотрим на результат:

![](https://dl.dropbox.com/u/381385/devblog/images/article_01/image_05.png)

Что касается еще одной важной функции, поддержки черновиков, то она есть в Docpad из коробки. Просто добавляем в блок метаинформации одной из статей следующий параметр:

    draft: true
    
И теперь при попытке зайти на данную страницу мы увидим сообщение о 404 ошибке.

Итак, теперь нам необходимо реализовать возможность показа списка статей на главной странице блога. Сперва объявим коллекцию `posts`, для чего в файле `docpad.coffee` ниже строки `# Collections` добавим следующий код:

    # Collections

    collections:
        posts: ->
            @getCollection('documents').findAllLive({relativeOutDirPath:'posts'}, [date:-1])

Теперь добавим в файл `documents/index.html.eco` следующий код:

    <% for document in @getCollection('posts').toJSON(): %>
    <article id="post" class="post">
        <a href="<%= document.url %>"><h1><%= document.title %></h1></a>
        <h6><%= document.author %> | <%= document.date.toShortDateString() %></h6>
        <div class="post-content"><%- document.content %></div>
    </article>
    <% end %>
     
И теперь у нас на главной странице появился список статей:

![](https://dl.dropbox.com/u/381385/devblog/images/article_01/image_06.png)

У выбранного нами подхода есть только один недостаток - все статьи отображаются в одном большом списке. Чтобы через некоторое время страница не начала грузиться слишком медленно нам необходимо реализовать пейджинацию. Для этого будем использовать плагин [docpad-plugin-paged](https://github.com/docpad/docpad-plugin-paged/). Дописываем в файл `package.json` в раздел `dependencies` следующую запись:

    "docpad-plugin-paged": "x.x"
    
Устанавливаем библиотеку:

    $ npm install
    
Теперь перепишем шаблон `index.html.eco` следующим образом:

    ---
    layout: 'default'
    isPaged: true
    pageOrder: 0
    pagedCollection: 'posts'
    pageSize: 3
    ---
    <% posts = @getCollection('posts') %>
    <% for i in [@document.page.startIdx...@document.page.endIdx]: %>
        <% document = posts.at(i).toJSON() %>
        <article id="post" class="post">
            <a href="<%= document.url %>"><h1><%= document.title %></h1></a>
            <h6><%= document.author %> | <%= document.date.toShortDateString() %></h6>
            <div class="post-content"><%- document.contentRenderedWithoutLayouts %></div>
        </article>
    <% end %>
    
    <% if @document.page.count > 1: %>
    <div class="pagination pull-right">
        <ul>
            <% if !@getDocument().hasPrevPage(): %>
                <li class="disabled"><span>&larr;</span></li>
            <% else: %>
                <li><a href="<% if @document.page.number-1 > 0: %>index.<%= @document.page.number-1 %>.html<% else: %>/<% end %>">&larr;</a></li>
            <% end %>
            <% for num in [0..@document.page.count-1]: %>
                <% if @document.page.number == num: %>
                    <li class="active"><span><%= num %></span></li>
                <% else: %>
                    <li><a href="<% if num > 0: %>index.<%= num %>.html<% else: %>/<% end %>"><%= num %></a></li>
                <% end %>
            <% end %>
            <% if !@getDocument().hasNextPage(): %>
                <li class="disabled"><span>&rarr;</span></li>
            <% else: %>
                <li><a href="index.<%= @document.page.number+1 %>.html">&rarr;</a></li>
            <% end %>
        </ul>
    </div>
    <% end %>

В результате посты будут разбиты по страницам, по 3 поста на каждой. В нижней же части страницы появится пейджинатор:

![](https://dl.dropbox.com/u/381385/devblog/images/article_01/image_07.png)

Но, что если статьи в блоге будут большими? Если не предусмотреть возможность использования ката, то страницы со списком статей могут быть очень большими. Ну, что ж, идем в `docpad.coffee` и пишем две новых функции для использования в шаблонах (чуть ниже блока `Helper Functions`):

        getCuttedContent: (content) ->            
            i = content.search('<!-- Read more -->')
            if i >= 0
                content[0..i-1]                
            else
                content

        hasReadMore: (content) ->
            content.search('<!-- Read more -->') >= 0

А код отвечающий за отображение статьи в шаблоне `index.html.eco` изменим следующим обазом:

    <article id="post" class="post">
        <a href="<%= document.url %>"><h1><%= document.title %></h1></a>
        <h6><%= document.author %> | <%= document.date.toShortDateString() %></h6>
        <div class="post-content"><%- @getCuttedContent(String(document.contentRenderedWithoutLayouts)) %></div>
        <% if @hasReadMore(String(document.contentRenderedWithoutLayouts)): %>
        <p><a href="<%= document.url %>"><strong>Читать дальше &rarr;</strong></a></p>
        <% end %>
    </article>

Вот и все, теперь если вставить в тело статьи строку `<!-- Read more -->`, то движок воспримет ее как разделитель - при генерации списка статей будет отображаться только часть статьи перед данной строкой, а в конце будет добавлена ссылка "Читать дальше" указывающая на страницу статьи. Если же ее нет, то статья будет отображаться полностью.

![](https://dl.dropbox.com/u/381385/devblog/images/article_01/image_08.png)

Добавим теперь страничку "Контакты", что довольно просто. Создадим сначала универсальный шаблон для страниц 'layouts/page.html.eco`:

    ---
    layout: default
    ---
    
    <article id="post" class="post">
        <h1><%= @document.title %></h1>
        <div class="post-content"><%- @content %></div>
    </article>

А теперь можно создавать и саму страницу `documents/contacts.html.md`:

    ---
    title: 'Контакты'
    layout: page
    ---
    
    Со мной всегда можно связаться по электронной почте: *oleh.korkh@the7bits.com*
    
Еще нам было бы неплохо добавить на данную страницу форму обратной связи. Поскольку наш блог после генерации являет собой просто набор статических HTML файлов, то для добавления динамических элементов нам придется воспользоваться сторонними сервисами. В данном случае воспользуемся сервисом [123contactform.com](http://www.123contactform.com/). Бесплатный тарифный план подразумевает возможность создания 5 форм и лимит в 100 сабмитов на месяц. На данном этапе этого достаточно, поэтому просто регистрируемся и создаем форму.

![](https://dl.dropbox.com/u/381385/devblog/images/article_01/image_09.png)

Выбираем вставку через iframe и вставляем полученный код для вставки в конец файла `documents/contact.html.md`:

    <!-- 123ContactForm.com script begins here -->
    <iframe style="height:600px; overflow:auto;" width="100%" id="contactform123" name="contactform123" marginwidth="0" marginheight="0" frameborder="0" src="http://www.123contactform.com/my-contact-form-korkholeh-458723.html">
    <p>Your browser does not support iframes. The contact form cannot be displayed. Please use another contact method (phone, fax etc)</p>
    </iframe>
    
    <p><a class="footerLink13" href="http://www.123contactform.com" title="Contact form generator">Contact form generator</a> powered by 123ContactForm.com | <a class="footerLink13" title="Looks like phishing? Report it!" href="http://www.123contactform.com/sf.php?s=123contactform-52&control119314=http://www.123contactform.com/contact-form--458723.html&control190=Report%20abuse" rel="nofollow">Report abuse</a></p><!-- 123ContactForm.com script ends here -->

Получим что-то такое:

![](https://dl.dropbox.com/u/381385/devblog/images/article_01/image_10.png)
    
Конечно любой нормальный блог должен также иметь систему комментариев. Для этого воспользуемся [Disqus][disqus]. Добавляем новый сайт указав в качестве короткого имени `7bits-devblog`. Получив код для вставки просто вставляем его в соответствующие шаблоны. Ленту сообщений вставляем в шаблон `post.html.eco`.

![](https://dl.dropbox.com/u/381385/devblog/images/article_01/image_11.png)

Также добавляем необходимый код в `index.html.eco` и таки образом добавляем вывод количества комментариев в заголовке поста.

Итак, мы имеем уже полноценный движок для блога, осталось только навести мелкий лоск. Для начала, добавим RSS ленту. Создадим новый файл `documents/atom.xml.eco`:

    <?xml version="1.0" encoding="utf-8"?>
    <feed xmlns="http://www.w3.org/2005/Atom">
        <title><%= @site.title %></title>
        <subtitle><%= @site.description %></subtitle>
        <link href="http://balupton.com/atom.xml" rel="self" />
        <link href="http://balupton.com" />
        <updated><%= @site.date.toISOString() %></updated>
        <id><%= @site.url %></id>
        <author>
            <name><%= @site.author %></name>
            <email><%= @site.email %></email>
        </author>
    
        <% for document in @getCollection('posts').toJSON(): %>
            <entry>
                <title><%= document.title or document.name or @title.title %></title>
                <link href="<%= @site.url %><%= document.url %>"/>
                <updated><%= document.date.toISOString() %></updated>
                <id><%= @site.url %><%= document.url %></id>
                <content type="html"><%= document.contentRenderedWithoutLayouts %></content>
            </entry>
        <% end %>
    </feed>

Создадим также файл `robots.txt` и выключим пока индексацию. Его добавим в папку `files`.

    User-agent: *
    Disallow: /

Добавим также карту сайта. Для этого нам понадобится плагин [docpad-plugin-sitemap](https://github.com/benjamind/docpad-plugin-sitemap). Добавляем в `package.json` в раздел `dependencies` следующую запись:

    "docpad-plugin-sitemap": "x.x"
    
И устанавливаем плагин командой `npm install`.

Теперь добавляем внутри файла `docpad.coffee` дополнительные настройки:

    plugins:
        sitemap:
            cachetime: 600000
            changefreq: 'weekly'
            priority: 0.5
            
Вот и все, теперь файл `sitemap.xml` генерируется автоматически.

И напоследок, чтобы все курасить добавим также в файл `documents/scripts/script.js.coffee` следующую строку, которая добавит рамку вокруг всех картинок в постах:

    $('div.post-content img').addClass 'thumbnail'
     
Итак, мы написали собственный блогодвижок на основе генератора статичных сайтов [Docpad][docpad], теперь дело только за деплойментом готового блога на сервер (в данном случае на [Webfaction][webfaction]). Чтобы не залить в репозиторий лишнего, создадим файл `.gitignore`:

    lib-cov

    pids
    logs
    results

    node_modules
    npm-debug.log

    env.coffee
    .env

Теперь окончательно сгенерируем весь сайт, закоммитим наши изменения и добавим их в удаленный репозиторий.

    $ docpad generate --env static    
    $ git add .
    $ git commit -m "initial commit"
    $ git push origin master

После того как код сайта залился в удаленный репозиторий, пойдем в его настройки на Bitbucket'е и в разделе "Deployment keys" добавим ключ нашего сервера.

![](https://dl.dropbox.com/u/381385/devblog/images/article_01/image_12.png)

Далее, заходим в админку аккаунта на [Webfaction][webfaction] и добавляем новый сайт, в качестве приложения указываем `Static -> Static/CGI/PHP-5.3`.

![](https://dl.dropbox.com/u/381385/devblog/images/article_01/image_13.png)

Теперь логинимся на сервер по SSH и копируем во временную папку файлы нашего сайта:

    $ git clone git@bitbucket.org:the7bits/devblog.website.git

Далее нам останется только скопировать содержимое папки `out` в папку `webapps/7b_devblog` и можно смотреть на результат:

![](https://dl.dropbox.com/u/381385/devblog/images/article_01/image_14.png)

Вот и все. Теперь, чтобы разместить в блоге новую запись нужно сделать следующее:

* Добавляем в папку `documents/posts` новый файл с текстом в формате [Markdown][markdown];
* Генерируем статический контент: `docpad generate --env static`;
* Добавляем новый файл к репозиторию, коммитим и делаем `git push`;
* На сервере делаем `git pull` и копируем сожержимое папки `out` в нужное место.

В одной из следующих статей рассмотрим вопрос о том, как можно данный процесс максимально  автоматизировать. Stay tuned!
    
[docpad]:http://docpad.org/
[bitbucket]:https://bitbucket.org/
[nodejs]:http://nodejs.org/
[eco]:https://github.com/sstephenson/eco
[markdown]:http://daringfireball.net/projects/markdown/syntax
[bootstrap]:http://twitter.github.com/bootstrap/
[jquery]:http://jquery.com/
[lorem]:http://www.lipsum.com/
[disqus]:http://disqus.com/
[webfaction]:http://webfaction.com/
