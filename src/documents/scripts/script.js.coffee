$ ->
    $('div.post-content img').each ->
        $(@).addClass 'thumbnail'
        src = $(@).attr 'src'
        a = $('<a data-jkit="[lightbox:group=images]"/>').attr 'href', src
        $(@).wrap a
    $('body').jKit()

